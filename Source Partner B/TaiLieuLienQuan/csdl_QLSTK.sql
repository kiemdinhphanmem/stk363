create database QLSTK
go
USE [QLSTK]
GO
/****** Object:  Table [dbo].[KhachHang]    Script Date: 11/4/2018 13:09:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KhachHang](
	[Email] [nvarchar](100) NOT NULL,
	[MatKhau] [varchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NganHang]    Script Date: 11/4/2018 13:09:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NganHang](
	[MaNH] [varchar](100) NOT NULL,
	[TenNH] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[MaNH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SoTietKiem]    Script Date: 11/4/2018 13:09:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SoTietKiem](
	[MaSoTK] [int] IDENTITY(1,1) NOT NULL,
	[MaNH] [varchar](100) NULL,
	[NgayGui] [datetime] NULL CONSTRAINT [DF__SoTietKie__NgayG__276EDEB3]  DEFAULT (getdate()),
	[NgayHetHan] [datetime] NULL,
	[SoTienGui] [float] NULL,
	[KyHanGui] [nvarchar](100) NULL,
	[LaiSuatNam] [float] NULL,
	[LaiSuatKhongKyHan] [float] NULL CONSTRAINT [DF__SoTietKie__LaiSu__286302EC]  DEFAULT ((0.05)),
	[KhiDenHan] [nvarchar](255) NULL,
	[TraLai] [nvarchar](255) NULL,
	[Email] [nvarchar](100) NULL,
	[Sotiendu] [float] NULL,
	[TatToan] [bit] NULL,
 CONSTRAINT [PK__SoTietKi__A61D4100640027CF] PRIMARY KEY CLUSTERED 
(
	[MaSoTK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[KhachHang] ([Email], [MatKhau]) VALUES (N'ddd@gmail .com', N'eeee')
INSERT [dbo].[KhachHang] ([Email], [MatKhau]) VALUES (N'ddd@gmail.com', N'111111111')
INSERT [dbo].[KhachHang] ([Email], [MatKhau]) VALUES (N'dinhnguyen@gmail.com', N'123')
INSERT [dbo].[KhachHang] ([Email], [MatKhau]) VALUES (N'dn@gmail.com', N'222222222')
INSERT [dbo].[KhachHang] ([Email], [MatKhau]) VALUES (N'khoa@gmail.com', N'123')
INSERT [dbo].[KhachHang] ([Email], [MatKhau]) VALUES (N'khoa@my.com', N'111111111')
INSERT [dbo].[KhachHang] ([Email], [MatKhau]) VALUES (N'my@gmail.com', N'11111111')
INSERT [dbo].[KhachHang] ([Email], [MatKhau]) VALUES (N'ndn2596@gmail.com', N'aa')
INSERT [dbo].[KhachHang] ([Email], [MatKhau]) VALUES (N'ntn1928@gmail.com', N'1111')
INSERT [dbo].[KhachHang] ([Email], [MatKhau]) VALUES (N'nguyen2510@gmail.com', N'222')
INSERT [dbo].[KhachHang] ([Email], [MatKhau]) VALUES (N'nguyendinhnguyen2510@gmail.com', N'nguyen96')
INSERT [dbo].[KhachHang] ([Email], [MatKhau]) VALUES (N'pham@gmail.com', N'11111111')
INSERT [dbo].[KhachHang] ([Email], [MatKhau]) VALUES (N'phamkhoa@gmail.com', N'123')
INSERT [dbo].[NganHang] ([MaNH], [TenNH]) VALUES (N'ACB', N'Ngân hàng Á Châu')
INSERT [dbo].[NganHang] ([MaNH], [TenNH]) VALUES (N'TPB', N'TPBank')
INSERT [dbo].[NganHang] ([MaNH], [TenNH]) VALUES (N'VCB', N'Vietcombank')
SET IDENTITY_INSERT [dbo].[SoTietKiem] ON 

INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (1, N'ACB', CAST(N'2018-11-02 00:00:00.000' AS DateTime), CAST(N'2019-05-02 00:00:00.000' AS DateTime), 1000006.8469338731, N'không kỳ hạn', 5, 0.05, N'Tái tục gốc', N'Cuối kỳ', N'pham@gmail.com', 5000000, 1)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (2, N'ACB', CAST(N'2018-10-13 00:00:00.000' AS DateTime), CAST(N'2019-04-13 00:00:00.000' AS DateTime), 5002500.3125, N'6 tháng', 5, 0.05, N'Tái tục gốc', N'Cuối kỳ', N'pham@gmail.com', 4000000, 1)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (3, N'VCB', CAST(N'2018-01-01 00:00:00.000' AS DateTime), CAST(N'2019-01-01 00:00:00.000' AS DateTime), 8100000, N'12 tháng', 6, 0.05, N'Tái tục gốc', N'Cuối kỳ', N'pham@gmail.com', 2000000, 0)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (4, N'VCB', CAST(N'2018-01-04 00:00:00.000' AS DateTime), CAST(N'2019-01-04 00:00:00.000' AS DateTime), 6000000, N'12 tháng', 8, 0.05, N'Tái tục gốc', N'Cuối kỳ', N'ddd@gmail .com', 2000000, 0)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (5, N'TPB', CAST(N'2018-01-01 00:00:00.000' AS DateTime), CAST(N'2018-04-01 00:00:00.000' AS DateTime), 7000000, N'3 tháng', 5, 0.05, N'Tái tục gốc', N'Cuối kỳ', N'pham@gmail.com', 1000000, 0)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (6, N'ACB', CAST(N'2018-09-02 00:00:00.000' AS DateTime), CAST(N'2018-12-02 00:00:00.000' AS DateTime), 5001250.078125, N'3 tháng', 6, 0.05, N'Tái tục gốc', N'Cuối kỳ', N'pham@gmail.com', 1000000, 1)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (7, N'ACB', CAST(N'2018-10-17 00:00:00.000' AS DateTime), CAST(N'2019-01-17 00:00:00.000' AS DateTime), 4001500.1875078124, N'3 tháng', 6, 0.05, N'Tái tục gốc và lãi', N'Cuối kỳ', N'pham@gmail.com', NULL, 1)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (8, N'TPB', CAST(N'2018-11-02 00:00:00.000' AS DateTime), CAST(N'2019-02-02 00:00:00.000' AS DateTime), 6000000, N'3 tháng', 6, 0.05, N'Tái tục gốc và lãi', N'Cuối kỳ', N'pham@gmail.com', NULL, 0)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (9, N'ACB', CAST(N'2017-12-01 00:00:00.000' AS DateTime), CAST(N'2018-03-01 00:00:00.000' AS DateTime), 7211575, N'3 tháng', 6, 0.05, N'Tái tục gốc và lãi', N'Cuối kỳ', N'pham@gmail.com', NULL, 1)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (10, N'ACB', CAST(N'2018-09-02 00:00:00.000' AS DateTime), CAST(N'2018-10-02 00:00:00.000' AS DateTime), 4033402.7777777775, N'1 tháng', 5, 0.05, N'Tái tục gốc', N'Cuối kỳ', N'pham@gmail.com', NULL, 0)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (11, N'TPB', CAST(N'2018-11-02 00:00:00.000' AS DateTime), CAST(N'2019-02-02 00:00:00.000' AS DateTime), 3500000, N'3 tháng', 5, 0.05, N'Tái tục gốc và lãi', N'Cuối kỳ', N'pham@gmail.com', NULL, 0)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (12, N'ACB', CAST(N'2018-11-02 00:00:00.000' AS DateTime), CAST(N'2018-12-02 00:00:00.000' AS DateTime), 2100087.5, N'1 tháng', 5.5, 0.05, N'Tái tục gốc và lãi', N'Cuối kỳ', N'pham@gmail.com', NULL, 0)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (13, N'ACB', CAST(N'2018-11-02 00:00:00.000' AS DateTime), CAST(N'2018-12-02 00:00:00.000' AS DateTime), 1000003.414221499, N'không kỳ hạn', 6, 0.05, N'Tái tục gốc và lãi', N'Cuối kỳ', N'pham@gmail.com', NULL, 0)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (14, N'ACB', CAST(N'2018-11-03 00:00:00.000' AS DateTime), CAST(N'2018-12-03 00:00:00.000' AS DateTime), 2000083.3333333333, N'1 tháng', 5, 0.05, N'Tái tục gốc và lãi', N'Cuối kỳ', N'pham@gmail.com', NULL, 0)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (15, N'ACB', CAST(N'2018-11-03 00:00:00.000' AS DateTime), CAST(N'2019-02-03 00:00:00.000' AS DateTime), 3000375, N'3 tháng', 5, 0.05, N'Tái tục gốc và lãi', N'Cuối kỳ', N'pham@gmail.com', NULL, 0)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (16, N'ACB', CAST(N'2018-11-03 00:00:00.000' AS DateTime), CAST(N'2019-02-03 00:00:00.000' AS DateTime), 3000375, N'3 tháng', 5, 0.05, N'Tái tục gốc và lãi', N'Cuối kỳ', N'pham@gmail.com', NULL, 0)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (17, N'TPB', CAST(N'2018-11-03 00:00:00.000' AS DateTime), NULL, 5000000, N'không kỳ hạn', 6, 0.05, N'Tái tục gốc và lãi', N'Cuối kỳ', N'khoa@my.com', NULL, 0)
INSERT [dbo].[SoTietKiem] ([MaSoTK], [MaNH], [NgayGui], [NgayHetHan], [SoTienGui], [KyHanGui], [LaiSuatNam], [LaiSuatKhongKyHan], [KhiDenHan], [TraLai], [Email], [Sotiendu], [TatToan]) VALUES (18, N'ACB', CAST(N'2018-11-03 00:00:00.000' AS DateTime), CAST(N'2018-12-03 00:00:00.000' AS DateTime), 1500000, N'1 tháng', 5, 0.05, N'Tất toán sổ', N'Định kỳ hàng tháng', N'khoa@my.com', NULL, 0)
SET IDENTITY_INSERT [dbo].[SoTietKiem] OFF
ALTER TABLE [dbo].[SoTietKiem]  WITH CHECK ADD  CONSTRAINT [CK__SoTietKie__NgayG__2B3F6F97] CHECK  (([NgayGui]<=getdate()))
GO
ALTER TABLE [dbo].[SoTietKiem] CHECK CONSTRAINT [CK__SoTietKie__NgayG__2B3F6F97]
GO
ALTER TABLE [dbo].[SoTietKiem]  WITH CHECK ADD  CONSTRAINT [CK__SoTietKie__SoTie__2C3393D0] CHECK  (([SoTienGui]>=(1000000)))
GO
ALTER TABLE [dbo].[SoTietKiem] CHECK CONSTRAINT [CK__SoTietKie__SoTie__2C3393D0]
GO
