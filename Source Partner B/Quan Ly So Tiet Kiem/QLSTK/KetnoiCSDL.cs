﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace QLSTK
{
    class KetnoiCSDL
    {
        SqlConnection sqlconn;
        public KetnoiCSDL(string svrname, string dbname, bool intergratedMode, string usernames, string pwd)
        {
            string connStr;
            svrname = "HP-ELITEBOOK-87\\SQLEXPRESS";
            if (intergratedMode == true)
                connStr = "server =" + svrname + "; database =" + dbname + "; Integrated Security = true";
            else
                connStr = "server =" + svrname + "; uid =" + usernames + "; pwd =" + pwd + "; database = " + dbname;
            sqlconn = new SqlConnection(connStr);
        }
        public DataTable Execute(string strquerry)
        {
            SqlDataAdapter da;
            da = new SqlDataAdapter(strquerry, sqlconn);
            DataTable dt = new DataTable();
            try
            {
               
                da.Fill(dt);
                
            }
            catch
            {

            }
            return dt;

        }
        public void ExecuteNonQuerry(string strquerry)
        {
            SqlCommand comm = new SqlCommand(strquerry, sqlconn);
            sqlconn.Open();
            comm.ExecuteNonQuery();
            sqlconn.Close();
        }
        public DataTable HienThiDL(string strquerry)
        {
            SqlDataAdapter da;
            da = new SqlDataAdapter(strquerry, sqlconn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            SqlCommand comm = new SqlCommand(strquerry, sqlconn);
            sqlconn.Open();
            comm.ExecuteNonQuery();
            sqlconn.Close();
            return dt;
        }
    }
}
