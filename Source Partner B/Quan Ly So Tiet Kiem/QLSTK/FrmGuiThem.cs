﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTK
{
    public partial class FrmGuiThem : Form
    {
        String strConnection = "Data Source=.\\SQLEXPRESS;Initial Catalog=QLSTK;Integrated Security=True";
        SqlConnection cnn = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=QLSTK;Integrated Security=True");
        SqlConnection conn;
        SqlCommand command;

        public string MaSoTK { get; set; }
        public string TenNganHang { get; set; }
        public double Tongsotiengoc { get; set; }
        public FrmGuiThem()
        {
            InitializeComponent();
        }

        public bool KiemTraNhapSoTienGuiThem(string nhapsotiengui)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            if (regex.IsMatch(nhapsotiengui))
            {
                double sotiengui = double.Parse(nhapsotiengui);
                if (sotiengui > 100000)
                {
                    return true;
                }

                else
                {
                    MessageBox.Show("Số tiền gửi thêm tối thiểu là một trăm nghìn(100000)!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            else if(string.IsNullOrEmpty(nhapsotiengui))
            {
                MessageBox.Show("Số tiền gửi thêm không được bỏ trống!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                MessageBox.Show("Số tiền gửi thêm phải nhập bằng số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return false;
        }

        private void btnGuiThem_Click(object sender, EventArgs e)
        {
            string sql = "UPDATE SoTietKiem SET SoTienGui = @sotiengui + @tienguithem WHERE MaSoTK = @idsotk";
            
            if (KiemTraNhapSoTienGuiThem(txtSoTienGui.Text)==true)
            {
                try
                {
                    conn = new SqlConnection(strConnection);
                    conn.Open();
                    command = new SqlCommand(sql, conn);
                    command.Parameters.Add(new SqlParameter("@sotiengui", Tongsotiengoc));
                    command.Parameters.Add(new SqlParameter("@tienguithem", txtSoTienGui.Text));
                    command.Parameters.Add(new SqlParameter("@idsotk", MaSoTK));
                    command.ExecuteScalar();

                    double sotiengui = double.Parse(txtSoTienGui.Text.ToString());
                    double TienXuatRa = Tongsotiengoc + sotiengui;

                    MessageBox.Show("Gửi thêm " + txtSoTienGui.Text + " VNĐ thành công! Tổng tiền hiện có: " + TienXuatRa + " VNĐ");
                    this.DialogResult = DialogResult.Cancel; //dong form
                }
                catch
                {

                }
                
            }
            
        }

        private void FrmGuiThem_Load(object sender, EventArgs e)
        {
            lblMaSo.Text = "Mã sổ tiết kiệm: " + MaSoTK;
            lblNganHang.Text = "Ngân hàng: "+TenNganHang;
            lblSoTienGoc.Text= "Số tiền trong sổ: " + Tongsotiengoc.ToString();
        }

        //private void txtSoTienGui_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
        //    {
        //        MessageBox.Show("Số tiền phải được nhập bằng số. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
        //        e.Handled = true;
        //    }

        //}

        private void lblSoTienGoc_Click(object sender, EventArgs e)
        {

        }

        private void btnHuy_Click(object sender, EventArgs e)
        {

            Close();
        }
    }
}
