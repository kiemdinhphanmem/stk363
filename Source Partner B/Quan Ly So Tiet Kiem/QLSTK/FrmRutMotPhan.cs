﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTK
{
    public partial class FrmRutMotPhan : Form
    {
        
        public string MaSoTK { get; set; }
        public string TenNganHang { get; set; }
        public double Tongsotiengoc { get; set; }
        public string KyHanGui { get; set; }

        public DateTime NgayGuiTien { get; set; }
        public DateTime NgayHetHan { get; set; }

        Sotietkiem stk = new Sotietkiem();
        KetnoiCSDL kn;
        public FrmRutMotPhan()
        {
            InitializeComponent();
            kn = new KetnoiCSDL(".\\SQLEXPRESS", "QLSTK", true, "", "");
        }

        private void FrmRutMotPhan_Load(object sender, EventArgs e)
        {

            lblMaSo.Text = "Mã sổ tiết kiệm: " + MaSoTK;
            lblNganHang.Text = "Ngân hàng: " + TenNganHang;
            lblSoTienGoc.Text = "Số tiền trong sổ: " + Tongsotiengoc.ToString();
            lblKyHanGui.Text = "Kỳ hạn gửi: " + KyHanGui;
            lblNgayGui.Text = "Ngày gửi: " + NgayGuiTien.ToString();
            lblNgayHetHan.Text = "Ngày hết hạn: " + NgayHetHan.ToString();
        }


        public bool KiemTraNhapSoTienRut(string nhapsotienrut)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            if (!regex.IsMatch(nhapsotienrut))
            {
                MessageBox.Show("Số tiền rút phải nhập bằng số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (string.IsNullOrEmpty(nhapsotienrut))
            {
                MessageBox.Show("Số tiền rút không được bỏ trống!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else
            {
                
            }
            return true;
        }
        private void btnRutTien_Click(object sender, EventArgs e)
        {

            
            if(KiemTraNhapSoTienRut(txtSoTienRut.Text))
            {
                TimeSpan songaygui_time = DateTime.Now - NgayGuiTien; //tinh tong so ngay gui tien bang cong thuc ngayhethan-ngayhientai
                string songaygui_hientai = Convert.ToString(songaygui_time.TotalDays); //lay tong so ngay gui tien ra string
                double songay = double.Parse(songaygui_hientai);
                double sotienrut = double.Parse(txtSoTienRut.Text.ToString());
                double TienXuatRa = Tongsotiengoc - sotienrut;
                if (double.Parse(txtSoTienRut.Text) > Tongsotiengoc)
                {
                    MessageBox.Show("Số tiền rút phải ít hơn số tiền hiện có trong sổ ("+Tongsotiengoc+")!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                else if (KyHanGui == "không kỳ hạn") //nếu là sổ tiết kiệm ko kỳ hạn thì tính theo công thưc riêng
                {

                    if (songay < 15)
                    {
                        MessageBox.Show("Số tiết kiệm này chưa gửi đủ 15 ngày. Đối với sổ tiết kiệm KHÔNG KỲ HẠN, chỉ được rút tiền khi gửi từ 15 ngày trở lên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {

                        stk.Rut1Phan(txtSoTienRut.Text.ToString(), MaSoTK);
                        MessageBox.Show("Rút " + txtSoTienRut.Text + " VNĐ thành công! Tổng tiền còn lại: " + TienXuatRa + " VNĐ");
                        this.DialogResult = DialogResult.Cancel; //dong form
                    }
                }

                else
                {
                    if (DateTime.Now < NgayHetHan)
                    {
                        DialogResult rs1 = MessageBox.Show("Sổ tiết kiệm " + TenNganHang + "_" + MaSoTK + " đến hạn ngày " + NgayHetHan.ToString() + ". Số tiền rút ra trước hạn sẽ được tính theo lãi suất không kỳ hạn(0.05%/năm). Bạn có muốn tiếp tục?", "Chú ý", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (rs1 == DialogResult.Yes)
                        {
                            stk.Rut1Phan(txtSoTienRut.Text.ToString(), MaSoTK);
                            MessageBox.Show("Rút " + txtSoTienRut.Text + " VNĐ thành công! Tổng tiền còn lại: " + TienXuatRa + " VNĐ");
                            this.DialogResult = DialogResult.Cancel; //dong form

                        }

                    }
                    else //ngược lại
                    {

                        stk.Rut1Phan(txtSoTienRut.Text.ToString(), MaSoTK);
                        MessageBox.Show("Rút " + txtSoTienRut.Text + " VNĐ thành công! Tổng tiền còn lại: " + TienXuatRa + " VNĐ");
                        this.DialogResult = DialogResult.Cancel; //dong form

                    }
                }
            }

            

           
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            
            Hide();

        }
    }
}
