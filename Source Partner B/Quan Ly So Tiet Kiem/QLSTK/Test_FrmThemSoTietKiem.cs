﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Extensions.Forms;

namespace QLSTK
{    
        [TestFixture]
        class Test_FrmThemSoTietKiem
    {
            private FrmThemSoTietKiem frm;


        [SetUp]
        public void ChayFormThemSTK()
        {
            frm = new FrmThemSoTietKiem();
            frm.Show();
        }

        [Test]
        public void TestKhoiTaoFormSoTietKiem()
        {
            Assert.AreEqual("Thêm sổ tiết kiệm", frm.Text);
        }

        [Test]
        public void TestLabel_ThemSTK()
        {
            ChayFormThemSTK();

            LabelTester lblNgayGui = new LabelTester("label6", frm);
            Assert.AreEqual("Ngày gửi", lblNgayGui.Text);

            LabelTester lblKyHan = new LabelTester("label7", frm);
            Assert.AreEqual("Kỳ hạn", lblKyHan.Text);

            LabelTester lblLaiSuatKoKyHan = new LabelTester("label8", frm);
            Assert.AreEqual("Lãi suất không kỳ hạn", lblLaiSuatKoKyHan.Text);

            LabelTester lblKhiDenHan = new LabelTester("label2", frm);
            Assert.AreEqual("Khi đến hạn", lblKhiDenHan.Text);

            LabelTester lblNganHang = new LabelTester("label9", frm);
            Assert.AreEqual("Ngân hàng", lblNganHang.Text);

            LabelTester lblSoTienGui = new LabelTester("label3", frm);
            Assert.AreEqual("Số tiền gửi", lblSoTienGui.Text);

            LabelTester lblLaiSuat = new LabelTester("label4", frm);
            Assert.AreEqual("Lãi suất", lblLaiSuat.Text);

            LabelTester lblTraLai = new LabelTester("label5", frm);
            Assert.AreEqual("Trả lãi", lblTraLai.Text);



        }

        [Test]
        public void TestBtn_ThemSTK()
        {
            ChayFormThemSTK();

            ButtonTester btnThem = new ButtonTester("btnThem", frm);
            Assert.AreEqual("Thêm", btnThem.Text);

            ButtonTester btnHuy = new ButtonTester("btnHuy", frm);
            Assert.AreEqual("Hủy", btnHuy.Text);

        }


        [Test] // test so tien gui duoi 1 trieu
        public void Test_FrmThemSTK_NhapSoTienGui_UTC1()
        {
            Assert.AreEqual(false, frm.KiemTraNhapSoTienGui(100000.ToString()), "Số tiền gửi tối thiểu là một triệu!");
        }

        [Test] // test so tien gui tren 1 trieu
        public void Test_FrmThemSTK_NhapSoTienGui_UTC2()
        {
            Assert.AreEqual(true, frm.KiemTraNhapSoTienGui(2000000.ToString()));
        }

        [Test] // test so tien gui nhap chu va so
        public void Test_FrmThemSTK_NhapSoTienGui_UTC3()
        {
            Assert.AreEqual(false, frm.KiemTraNhapSoTienGui("1000000abc"), "Số tiền gửi phải nhập bằng số!");
        }

        [Test] // test so tien gui nhap chu
        public void Test_FrmThemSTK_NhapSoTienGui_UTC4()
        {
            Assert.AreEqual(false, frm.KiemTraNhapSoTienGui("abc"), "Số tiền gửi phải nhập bằng số!");
        }

        [Test] // test so tien gui bo trong
        public void Test_FrmThemSTK_NhapSoTienGui_UTC5()
        {
            Assert.AreEqual(false, frm.KiemTraTxtChuaNhap("5", ""), "Không được để trống Số Tiền Gửi!");
        }



        [Test] // test lai suat nhap so
        public void Test_FrmThemSTK_NhapLaiSuat_UTC1()
        {
            ComboBoxTester cbKyHan = new ComboBoxTester("cBKyHan", frm);
            if (cbKyHan.Text != "không kỳ hạn")
            {
                Assert.AreEqual(false, frm.KiemTraNhapLaiSuat("5abc", ""), "Lãi suất phải nhập bằng số!");
            }
        }

        [Test] // test lai suat/nam bo trong
        public void Test_FrmThemSTK_NhapLaiSuat_UTC2()
        {
            ComboBoxTester cbKyHan = new ComboBoxTester("cBKyHan", frm);
            if (cbKyHan.Text != "không kỳ hạn")
            {
                Assert.AreEqual(false, frm.KiemTraTxtChuaNhap("", "2000000"), "Không được để trống Lãi Suất!");
            }

        }


        [Test] // test lai suat khong ky han nhap chu va so
        public void Test_FrmThemSTK_NhapLaiSuatKhongKyHan_UTC1()
        {
            ComboBoxTester cbKyHan = new ComboBoxTester("cBKyHan", frm);
            if (cbKyHan.Text == "không kỳ hạn")
            {
                Assert.AreEqual(false, frm.KiemTraNhapLaiSuat("", "5abc"), "Lãi suất không kỳ hạn phải nhập bằng số!");
            }

        }

        [Test] // test lai suat khong ky han nhap chu
        public void Test_FrmThemSTK_NhapLaiSuatKhongKyHan_UTC2()
        {
            ComboBoxTester cbKyHan = new ComboBoxTester("cBKyHan", frm);
            if (cbKyHan.Text == "không kỳ hạn")
            {
                Assert.AreEqual(false, frm.KiemTraNhapLaiSuat("", "abc"), "Lãi suất không kỳ hạn phải nhập bằng số!");
            }

        }


        [Test] // test ngay gui tien lon hon ngay hien tai
        public void Test_FrmThemSTK_NgayGuiTien_UTC1()
        {
            Assert.AreEqual(false, frm.KiemTraNgayGui(DateTime.Now.AddDays(1).ToString()), "Ngày gửi phải nhỏ hoặc bằng hơn ngày hiện tại!");
        }



    }

}
