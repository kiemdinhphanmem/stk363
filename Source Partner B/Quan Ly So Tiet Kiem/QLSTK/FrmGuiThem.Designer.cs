﻿namespace QLSTK
{
    partial class FrmGuiThem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMaSoTk = new System.Windows.Forms.Label();
            this.btnGuiThem = new System.Windows.Forms.Button();
            this.btnHuy = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoTienGui = new System.Windows.Forms.TextBox();
            this.lblMaSo = new System.Windows.Forms.Label();
            this.lblSoTienGoc = new System.Windows.Forms.Label();
            this.lblNganHang = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblMaSoTk
            // 
            this.lblMaSoTk.AutoSize = true;
            this.lblMaSoTk.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblMaSoTk.Location = new System.Drawing.Point(-159, 42);
            this.lblMaSoTk.Name = "lblMaSoTk";
            this.lblMaSoTk.Size = new System.Drawing.Size(102, 16);
            this.lblMaSoTk.TabIndex = 71;
            this.lblMaSoTk.Text = "Mã sổ tiết kiệm:";
            // 
            // btnGuiThem
            // 
            this.btnGuiThem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnGuiThem.Location = new System.Drawing.Point(118, 131);
            this.btnGuiThem.Name = "btnGuiThem";
            this.btnGuiThem.Size = new System.Drawing.Size(75, 23);
            this.btnGuiThem.TabIndex = 70;
            this.btnGuiThem.Text = "Gửi thêm";
            this.btnGuiThem.UseVisualStyleBackColor = true;
            this.btnGuiThem.Click += new System.EventHandler(this.btnGuiThem_Click);
            // 
            // btnHuy
            // 
            this.btnHuy.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnHuy.Location = new System.Drawing.Point(206, 131);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(75, 23);
            this.btnHuy.TabIndex = 69;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.UseVisualStyleBackColor = true;
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label3.Location = new System.Drawing.Point(3, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 16);
            this.label3.TabIndex = 68;
            this.label3.Text = "Số tiền gửi thêm";
            // 
            // txtSoTienGui
            // 
            this.txtSoTienGui.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtSoTienGui.Location = new System.Drawing.Point(118, 83);
            this.txtSoTienGui.Name = "txtSoTienGui";
            this.txtSoTienGui.Size = new System.Drawing.Size(163, 22);
            this.txtSoTienGui.TabIndex = 67;
            // 
            // lblMaSo
            // 
            this.lblMaSo.AutoSize = true;
            this.lblMaSo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblMaSo.Location = new System.Drawing.Point(6, 9);
            this.lblMaSo.Name = "lblMaSo";
            this.lblMaSo.Size = new System.Drawing.Size(102, 16);
            this.lblMaSo.TabIndex = 72;
            this.lblMaSo.Text = "Mã sổ tiết kiệm:";
            // 
            // lblSoTienGoc
            // 
            this.lblSoTienGoc.AutoSize = true;
            this.lblSoTienGoc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblSoTienGoc.Location = new System.Drawing.Point(12, 60);
            this.lblSoTienGoc.Name = "lblSoTienGoc";
            this.lblSoTienGoc.Size = new System.Drawing.Size(96, 16);
            this.lblSoTienGoc.TabIndex = 74;
            this.lblSoTienGoc.Text = "Số tiền sẵn có:";
            this.lblSoTienGoc.Click += new System.EventHandler(this.lblSoTienGoc_Click);
            // 
            // lblNganHang
            // 
            this.lblNganHang.AutoSize = true;
            this.lblNganHang.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblNganHang.Location = new System.Drawing.Point(32, 34);
            this.lblNganHang.Name = "lblNganHang";
            this.lblNganHang.Size = new System.Drawing.Size(76, 16);
            this.lblNganHang.TabIndex = 75;
            this.lblNganHang.Text = "Ngân Hàng:";
            // 
            // FrmGuiThem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 171);
            this.ControlBox = false;
            this.Controls.Add(this.lblNganHang);
            this.Controls.Add(this.lblSoTienGoc);
            this.Controls.Add(this.lblMaSo);
            this.Controls.Add(this.lblMaSoTk);
            this.Controls.Add(this.btnGuiThem);
            this.Controls.Add(this.btnHuy);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtSoTienGui);
            this.Name = "FrmGuiThem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gửi Thêm";
            this.Load += new System.EventHandler(this.FrmGuiThem_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMaSoTk;
        private System.Windows.Forms.Button btnGuiThem;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSoTienGui;
        private System.Windows.Forms.Label lblMaSo;
        private System.Windows.Forms.Label lblSoTienGoc;
        private System.Windows.Forms.Label lblNganHang;
    }
}