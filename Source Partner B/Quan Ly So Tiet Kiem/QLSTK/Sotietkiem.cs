﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace QLSTK
{
    class Sotietkiem
    {
        KetnoiCSDL kn;
        public Sotietkiem()
        {
            kn = new KetnoiCSDL(".\\SQLEXPRESS", "QLSTK", true, "", "");
        }
        public DataTable LaySTK()
        {
            return kn.Execute("Select * from NganHang");
        }
        public DataTable LayAllSTK()
        {
            return kn.Execute("Select * from SoTietKiem");
        }
        public DataTable SotietkiemVCB()
        {
            return kn.Execute("Select MaSoTK,MaNH,NgayGui,NgayHetHan,SoTienGui,KyHanGui,LaiSuatNam,Sotiendu from SoTietKiem where MaNH = 'VCB'");
        }
        public DataTable DanhsachSotietkiemTheoNganHang(string @MaNH, string @mail)
        {
            return kn.Execute("Select MaSoTK,MaNH,NgayGui,NgayHetHan,SoTienGui,KyHanGui,LaiSuatNam,LaiSuatKhongKyHan,Sotiendu from SoTietKiem where TatToan=0 and MaNH = '"+@MaNH+"' and email='"+@mail+"'");
        }

        public DataTable DanhsachSotietkiem(string @mail)
        {
            return kn.Execute("Select MaSoTK,MaNH,NgayGui,NgayHetHan,SoTienGui,KyHanGui,LaiSuatNam,LaiSuatKhongKyHan,Sotiendu from SoTietKiem where TatToan=0 and email='" + @mail + "'");
        }
        public DataTable GetDemSoTatToan(string @mail)
        {
            return kn.Execute("select count(*) from SoTietKiem where TatToan=1 and email='" + @mail+ "'");
        }

        public DataTable GetDuLieuGanVaoUpdate(string id)
        {
            return kn.Execute("select NgayGui,KhiDenHan, TraLai from SoTietKiem where MaSoTK='" + id + "'");
        }



        public DataTable GetDanhsachDaTatToan(string @email)
        {
            return kn.Execute("Select MaSoTK,MaNH,NgayGui,NgayHetHan,SoTienGui,KyHanGui,LaiSuatNam,LaiSuatKhongKyHan,Sotiendu from SoTietKiem where TatToan=1 and email='" + @email + "'");
        }

        public DataTable GetTongTienTheoNH(string nh, string email)
        {
            return kn.Execute("Select sum(SoTienGui) from SoTietKiem where TatToan=0 and MaNH = '" + nh + "' and email='" + email + "'");
        }
        public DataTable TatToanTruocKyHan(string id)
        {
            
            return kn.Execute("UPDATE SoTietKiem SET SoTienGui = SoTienGui+(SoTienGui*(0.05/100)/12*DATEDIFF(month,NgayGui,NgayHetHan)), TatToan=1 WHERE MaSoTK='" + id + "'");
        }

        public DataTable TatToanSauKyHan(string id)
        {

            return kn.Execute("UPDATE SoTietKiem SET SoTienGui = SoTienGui+(SoTienGui*(LaiSuatNam/100)/12*DATEDIFF(month,NgayGui,NgayHetHan)), TatToan=1 WHERE MaSoTK='" + id + "'");
        }

        public DataTable TinhLaiCuoiKy()
        {
            return kn.Execute(
                "UPDATE SoTietKiem SET SoTienGui = SoTienGui+(SoTienGui*(LaiSuatNam/100)/12*DATEDIFF(month,NgayGui,NgayHetHan)) WHERE TraLai=N'Cuối kỳ' and TatToan=0");
        }

        public DataTable TatToanStkKhongKyHan(string songaygui_hientai,string id)
        {

            return kn.Execute("UPDATE SoTietKiem SET SoTienGui = SoTienGui+SoTienGui*(LaiSuatKhongKyHan/100)/360*"+songaygui_hientai+", TatToan=1 WHERE MaSoTK='" + id + "'");
        }


        public DataTable GetTongTienByEmail(string email)
        {
            return kn.Execute("Select sum(SoTienGui) from SoTietKiem where TatToan=0 and email='" + email + "'");
        }

        public DataTable Rut1Phan(string sotienrut, string id)
        {

            return kn.Execute("UPDATE SoTietKiem SET SoTienGui = SoTienGui-"+sotienrut+ "WHERE MaSoTK ="+id);


        }

        public DataTable GetThongTinSoTatToan(string id)
        {
            return kn.Execute("select * from SoTietKiem where MaSoTK = " + id);
        }

        public DataTable ThemNganHang(string id, string ten)
        {
            return kn.Execute("insert into NganHang values('"+id+"','"+ten+"')");
        }

        public DataTable XoaNganHang(string id)
        {
            return kn.Execute("delete from NganHang where MaNH='"+id+"'");
        }





    }
}
