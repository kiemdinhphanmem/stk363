﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTK
{
    public partial class FormDangKy : Form
    {
        String strConnection = "Data Source=.\\SQLEXPRESS;Initial Catalog=QLSTK;Integrated Security=True";
        SqlConnection conn;
        SqlCommand command;
        public static string UserName = "";
        public FormDangKy()
        {
            InitializeComponent();
            //kn = new KetnoiCSDL(".","QLSTK", true, "", "");
        }
        private void FormDangKy_Load(object sender, EventArgs e)
        {
            txt_MatKhau.Focus();
        }

        private void btndangky_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = "insert into KhachHang values(@email,@matkhau)";


                string checkEmail = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                Regex reg = new Regex(checkEmail);

                if (string.IsNullOrEmpty(txt_MatKhau.Text) && string.IsNullOrEmpty(txt_Email.Text))
                {
                    MessageBox.Show("Chưa nhập tài khoản và mật khẩu");
                }
                else if (string.IsNullOrEmpty(txt_Email.Text))
                {
                    MessageBox.Show("Chưa nhập email");
                }
                else if (string.IsNullOrEmpty(txt_MatKhau.Text))
                {
                    MessageBox.Show("Chưa nhập mật khẩu");
                }
                else if (!reg.IsMatch(txt_Email.Text))
                {
                    MessageBox.Show("Sai định dạng email!");
                }
                else if (txt_MatKhau.Text.Length < 8)
                {
                    MessageBox.Show("Mật khẩu tối thiểu có 8 ký tự!");
                }
                else
                {

                    try
                    {
                        conn = new SqlConnection(strConnection);
                        conn.Open();
                        command = new SqlCommand(sql, conn);
                        command.Parameters.Add(new SqlParameter("@email", txt_Email.Text));
                        command.Parameters.Add(new SqlParameter("@matKhau", txt_MatKhau.Text));
                        command.ExecuteScalar();
                        MessageBox.Show("Đăng ký thành công!");

                        FormSoTietKiem.UserName = txt_Email.Text; // gán txt_email chào mừng để gắn vào lbl chào mừng bên frm SoTietKiem 
                        Hide();
                        FormSoTietKiem frm = new FormSoTietKiem();
                        frm.Show();
                    }
                    catch
                    {
                        MessageBox.Show("Email đã tồn tại! Vui lòng nhập email khác!");
                    }

                }


            }
            catch
            {

            }
            finally
            {
                try { conn.Close(); }
                catch
                {

                }

            }

        }
        private void btndangnhap_Click(object sender, EventArgs e)
        {
            Hide();
            FormDangNhap frm = new FormDangNhap();
            frm.Show();
        }
        private void paneldangky_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txt_Email_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txt_Email_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_MatKhau_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_MatKhau_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void cbHienMatKhau_CheckedChanged(object sender, EventArgs e)
        {
            if (cbHienMatKhau.Checked)
                txt_MatKhau.UseSystemPasswordChar = false;
            else
                txt_MatKhau.UseSystemPasswordChar = true;
        }
    }
}
