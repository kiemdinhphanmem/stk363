﻿namespace QLSTK
{
    partial class FormSoTietKiem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDangXuat = new System.Windows.Forms.Button();
            this.btnSua = new System.Windows.Forms.Button();
            this.btnGuiThem = new System.Windows.Forms.Button();
            this.btnRutMotPhan = new System.Windows.Forms.Button();
            this.btnTatToan = new System.Windows.Forms.Button();
            this.dgvnganhang = new System.Windows.Forms.DataGridView();
            this.maNHDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maSoTKDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ngayGuiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ngayHetHanDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.soTienGuiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kyHanGuiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laiSuatNamDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laiSuatKhongKyHanDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.soTietKiemBindingSource14 = new System.Windows.Forms.BindingSource(this.components);
            this.qLSTKDataSet10 = new QLSTK.QLSTKDataSet10();
            this.cBNganHang = new System.Windows.Forms.ComboBox();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.btnThem = new System.Windows.Forms.Button();
            this.lbdemso = new System.Windows.Forms.Label();
            this.dgvTatToan = new System.Windows.Forms.DataGridView();
            this.maSoTKDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.soTienGuiDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kyHanGuiDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.laiSuatNamDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnXemSoTatToan = new System.Windows.Forms.DataGridViewButtonColumn();
            this.soTietKiemBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.qLSTKDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.qLSTKDataSet = new QLSTK.QLSTKDataSet();
            this.lblTatToan = new System.Windows.Forms.Label();
            this.lblTongTienNH = new System.Windows.Forms.Label();
            this.lblTongAllNganHang = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.soTietKiemBindingSource12 = new System.Windows.Forms.BindingSource(this.components);
            this.qLSTKDataSet8 = new QLSTK.QLSTKDataSet8();
            this.soTietKiemBindingSource9 = new System.Windows.Forms.BindingSource(this.components);
            this.qLSTKDataSet5 = new QLSTK.QLSTKDataSet5();
            this.soTietKiemBindingSource6 = new System.Windows.Forms.BindingSource(this.components);
            this.qLSTKDataSet2 = new QLSTK.QLSTKDataSet2();
            this.soTietKiemBindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.soTietKiemBindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.qLSTKDataSet1 = new QLSTK.QLSTKDataSet1();
            this.soTietKiemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.soTietKiemTableAdapter = new QLSTK.QLSTKDataSetTableAdapters.SoTietKiemTableAdapter();
            this.soTietKiemTableAdapter1 = new QLSTK.QLSTKDataSet1TableAdapters.SoTietKiemTableAdapter();
            this.soTietKiemTableAdapter2 = new QLSTK.QLSTKDataSet2TableAdapters.SoTietKiemTableAdapter();
            this.qlstkDataSet3 = new QLSTK.QLSTKDataSet();
            this.qlstkDataSet4 = new QLSTK.QLSTKDataSet();
            this.qLSTKDataSet31 = new QLSTK.QLSTKDataSet3();
            this.soTietKiemBindingSource7 = new System.Windows.Forms.BindingSource(this.components);
            this.soTietKiemTableAdapter3 = new QLSTK.QLSTKDataSet3TableAdapters.SoTietKiemTableAdapter();
            this.qLSTKDataSet41 = new QLSTK.QLSTKDataSet4();
            this.soTietKiemBindingSource8 = new System.Windows.Forms.BindingSource(this.components);
            this.soTietKiemTableAdapter4 = new QLSTK.QLSTKDataSet4TableAdapters.SoTietKiemTableAdapter();
            this.soTietKiemTableAdapter5 = new QLSTK.QLSTKDataSet5TableAdapters.SoTietKiemTableAdapter();
            this.qLSTKDataSet6 = new QLSTK.QLSTKDataSet6();
            this.soTietKiemBindingSource10 = new System.Windows.Forms.BindingSource(this.components);
            this.soTietKiemTableAdapter6 = new QLSTK.QLSTKDataSet6TableAdapters.SoTietKiemTableAdapter();
            this.qLSTKDataSet7 = new QLSTK.QLSTKDataSet7();
            this.soTietKiemBindingSource11 = new System.Windows.Forms.BindingSource(this.components);
            this.soTietKiemTableAdapter7 = new QLSTK.QLSTKDataSet7TableAdapters.SoTietKiemTableAdapter();
            this.soTietKiemTableAdapter8 = new QLSTK.QLSTKDataSet8TableAdapters.SoTietKiemTableAdapter();
            this.qLSTKDataSet9 = new QLSTK.QLSTKDataSet9();
            this.soTietKiemBindingSource13 = new System.Windows.Forms.BindingSource(this.components);
            this.soTietKiemTableAdapter9 = new QLSTK.QLSTKDataSet9TableAdapters.SoTietKiemTableAdapter();
            this.soTietKiemTableAdapter10 = new QLSTK.QLSTKDataSet10TableAdapters.SoTietKiemTableAdapter();
            this.sotietkiemBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.sotietkiemBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvnganhang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTatToan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qlstkDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qlstkDataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sotietkiemBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sotietkiemBindingSource3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnDangXuat);
            this.panel1.Controls.Add(this.btnSua);
            this.panel1.Controls.Add(this.btnGuiThem);
            this.panel1.Controls.Add(this.btnRutMotPhan);
            this.panel1.Controls.Add(this.btnTatToan);
            this.panel1.Controls.Add(this.dgvnganhang);
            this.panel1.Controls.Add(this.cBNganHang);
            this.panel1.Controls.Add(this.lblWelcome);
            this.panel1.Controls.Add(this.btnThem);
            this.panel1.Controls.Add(this.lbdemso);
            this.panel1.Controls.Add(this.dgvTatToan);
            this.panel1.Controls.Add(this.lblTatToan);
            this.panel1.Controls.Add(this.lblTongTienNH);
            this.panel1.Controls.Add(this.lblTongAllNganHang);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(862, 625);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btnDangXuat
            // 
            this.btnDangXuat.Location = new System.Drawing.Point(776, 6);
            this.btnDangXuat.Name = "btnDangXuat";
            this.btnDangXuat.Size = new System.Drawing.Size(75, 23);
            this.btnDangXuat.TabIndex = 27;
            this.btnDangXuat.Text = "Đăng Xuất";
            this.btnDangXuat.UseVisualStyleBackColor = true;
            this.btnDangXuat.Click += new System.EventHandler(this.btnDangXuat_Click);
            // 
            // btnSua
            // 
            this.btnSua.Location = new System.Drawing.Point(533, 82);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(75, 23);
            this.btnSua.TabIndex = 26;
            this.btnSua.Text = "Sửa";
            this.btnSua.UseVisualStyleBackColor = true;
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnGuiThem
            // 
            this.btnGuiThem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnGuiThem.Location = new System.Drawing.Point(614, 82);
            this.btnGuiThem.Name = "btnGuiThem";
            this.btnGuiThem.Size = new System.Drawing.Size(75, 23);
            this.btnGuiThem.TabIndex = 25;
            this.btnGuiThem.Text = "Gửi Thêm";
            this.btnGuiThem.UseVisualStyleBackColor = true;
            this.btnGuiThem.Click += new System.EventHandler(this.btnGuiThem_Click);
            // 
            // btnRutMotPhan
            // 
            this.btnRutMotPhan.Location = new System.Drawing.Point(695, 82);
            this.btnRutMotPhan.Name = "btnRutMotPhan";
            this.btnRutMotPhan.Size = new System.Drawing.Size(75, 23);
            this.btnRutMotPhan.TabIndex = 24;
            this.btnRutMotPhan.Text = "Rút 1 Phần";
            this.btnRutMotPhan.UseVisualStyleBackColor = true;
            this.btnRutMotPhan.Click += new System.EventHandler(this.btnRutMotPhan_Click);
            // 
            // btnTatToan
            // 
            this.btnTatToan.Location = new System.Drawing.Point(776, 82);
            this.btnTatToan.Name = "btnTatToan";
            this.btnTatToan.Size = new System.Drawing.Size(75, 23);
            this.btnTatToan.TabIndex = 23;
            this.btnTatToan.Text = "Tất Toán";
            this.btnTatToan.UseVisualStyleBackColor = true;
            this.btnTatToan.Click += new System.EventHandler(this.btnTatToan_Click);
            // 
            // dgvnganhang
            // 
            this.dgvnganhang.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvnganhang.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvnganhang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvnganhang.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.maNHDataGridViewTextBoxColumn,
            this.maSoTKDataGridViewTextBoxColumn,
            this.ngayGuiDataGridViewTextBoxColumn,
            this.ngayHetHanDataGridViewTextBoxColumn,
            this.soTienGuiDataGridViewTextBoxColumn,
            this.kyHanGuiDataGridViewTextBoxColumn,
            this.laiSuatNamDataGridViewTextBoxColumn,
            this.laiSuatKhongKyHanDataGridViewTextBoxColumn});
            this.dgvnganhang.DataSource = this.soTietKiemBindingSource14;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvnganhang.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvnganhang.Location = new System.Drawing.Point(11, 112);
            this.dgvnganhang.Margin = new System.Windows.Forms.Padding(2);
            this.dgvnganhang.Name = "dgvnganhang";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvnganhang.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvnganhang.RowTemplate.Height = 28;
            this.dgvnganhang.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvnganhang.Size = new System.Drawing.Size(840, 246);
            this.dgvnganhang.TabIndex = 22;
            this.dgvnganhang.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvnganhang_CellContentClick);
            // 
            // maNHDataGridViewTextBoxColumn
            // 
            this.maNHDataGridViewTextBoxColumn.DataPropertyName = "MaNH";
            this.maNHDataGridViewTextBoxColumn.HeaderText = "Ngân hàng";
            this.maNHDataGridViewTextBoxColumn.Name = "maNHDataGridViewTextBoxColumn";
            // 
            // maSoTKDataGridViewTextBoxColumn
            // 
            this.maSoTKDataGridViewTextBoxColumn.DataPropertyName = "MaSoTK";
            this.maSoTKDataGridViewTextBoxColumn.HeaderText = "Mã số tiết kiệm";
            this.maSoTKDataGridViewTextBoxColumn.Name = "maSoTKDataGridViewTextBoxColumn";
            this.maSoTKDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ngayGuiDataGridViewTextBoxColumn
            // 
            this.ngayGuiDataGridViewTextBoxColumn.DataPropertyName = "NgayGui";
            this.ngayGuiDataGridViewTextBoxColumn.HeaderText = "Ngày gửi";
            this.ngayGuiDataGridViewTextBoxColumn.Name = "ngayGuiDataGridViewTextBoxColumn";
            // 
            // ngayHetHanDataGridViewTextBoxColumn
            // 
            this.ngayHetHanDataGridViewTextBoxColumn.DataPropertyName = "NgayHetHan";
            this.ngayHetHanDataGridViewTextBoxColumn.HeaderText = "Ngày hết hạn";
            this.ngayHetHanDataGridViewTextBoxColumn.Name = "ngayHetHanDataGridViewTextBoxColumn";
            // 
            // soTienGuiDataGridViewTextBoxColumn
            // 
            this.soTienGuiDataGridViewTextBoxColumn.DataPropertyName = "SoTienGui";
            this.soTienGuiDataGridViewTextBoxColumn.HeaderText = "Số tiền gửi";
            this.soTienGuiDataGridViewTextBoxColumn.Name = "soTienGuiDataGridViewTextBoxColumn";
            // 
            // kyHanGuiDataGridViewTextBoxColumn
            // 
            this.kyHanGuiDataGridViewTextBoxColumn.DataPropertyName = "KyHanGui";
            this.kyHanGuiDataGridViewTextBoxColumn.HeaderText = "Kỳ hạn gửi";
            this.kyHanGuiDataGridViewTextBoxColumn.Name = "kyHanGuiDataGridViewTextBoxColumn";
            // 
            // laiSuatNamDataGridViewTextBoxColumn
            // 
            this.laiSuatNamDataGridViewTextBoxColumn.DataPropertyName = "LaiSuatNam";
            this.laiSuatNamDataGridViewTextBoxColumn.FillWeight = 70F;
            this.laiSuatNamDataGridViewTextBoxColumn.HeaderText = "Lãi suất năm";
            this.laiSuatNamDataGridViewTextBoxColumn.Name = "laiSuatNamDataGridViewTextBoxColumn";
            // 
            // laiSuatKhongKyHanDataGridViewTextBoxColumn
            // 
            this.laiSuatKhongKyHanDataGridViewTextBoxColumn.DataPropertyName = "LaiSuatKhongKyHan";
            this.laiSuatKhongKyHanDataGridViewTextBoxColumn.HeaderText = "Lãi suất không kỳ hạn (nếu có)";
            this.laiSuatKhongKyHanDataGridViewTextBoxColumn.Name = "laiSuatKhongKyHanDataGridViewTextBoxColumn";
            // 
            // soTietKiemBindingSource14
            // 
            this.soTietKiemBindingSource14.DataMember = "SoTietKiem";
            this.soTietKiemBindingSource14.DataSource = this.qLSTKDataSet10;
            // 
            // qLSTKDataSet10
            // 
            this.qLSTKDataSet10.DataSetName = "QLSTKDataSet10";
            this.qLSTKDataSet10.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cBNganHang
            // 
            this.cBNganHang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBNganHang.FormattingEnabled = true;
            this.cBNganHang.Items.AddRange(new object[] {
            "Tất cả"});
            this.cBNganHang.Location = new System.Drawing.Point(307, 82);
            this.cBNganHang.Margin = new System.Windows.Forms.Padding(2);
            this.cBNganHang.Name = "cBNganHang";
            this.cBNganHang.Size = new System.Drawing.Size(140, 21);
            this.cBNganHang.TabIndex = 21;
            this.cBNganHang.SelectedIndexChanged += new System.EventHandler(this.cBNganHang_SelectedIndexChanged);
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblWelcome.Location = new System.Drawing.Point(8, 6);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(59, 16);
            this.lblWelcome.TabIndex = 19;
            this.lblWelcome.Text = "Xin Chào";
            this.lblWelcome.Click += new System.EventHandler(this.lblWelcome_Click_1);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(452, 82);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(75, 23);
            this.btnThem.TabIndex = 18;
            this.btnThem.Text = "Thêm";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lbdemso
            // 
            this.lbdemso.AutoSize = true;
            this.lbdemso.Location = new System.Drawing.Point(367, 69);
            this.lbdemso.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbdemso.Name = "lbdemso";
            this.lbdemso.Size = new System.Drawing.Size(0, 13);
            this.lbdemso.TabIndex = 15;
            // 
            // dgvTatToan
            // 
            this.dgvTatToan.AutoGenerateColumns = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTatToan.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvTatToan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTatToan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.maSoTKDataGridViewTextBoxColumn2,
            this.soTienGuiDataGridViewTextBoxColumn2,
            this.kyHanGuiDataGridViewTextBoxColumn2,
            this.laiSuatNamDataGridViewTextBoxColumn2,
            this.btnXemSoTatToan});
            this.dgvTatToan.DataSource = this.soTietKiemBindingSource1;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTatToan.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvTatToan.Location = new System.Drawing.Point(11, 378);
            this.dgvTatToan.Margin = new System.Windows.Forms.Padding(2);
            this.dgvTatToan.Name = "dgvTatToan";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTatToan.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvTatToan.RowTemplate.Height = 28;
            this.dgvTatToan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTatToan.Size = new System.Drawing.Size(840, 236);
            this.dgvTatToan.TabIndex = 11;
            this.dgvTatToan.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgdatattoan_CellContentClick);
            // 
            // maSoTKDataGridViewTextBoxColumn2
            // 
            this.maSoTKDataGridViewTextBoxColumn2.DataPropertyName = "MaSoTK";
            this.maSoTKDataGridViewTextBoxColumn2.HeaderText = "Mã số";
            this.maSoTKDataGridViewTextBoxColumn2.Name = "maSoTKDataGridViewTextBoxColumn2";
            // 
            // soTienGuiDataGridViewTextBoxColumn2
            // 
            this.soTienGuiDataGridViewTextBoxColumn2.DataPropertyName = "SoTienGui";
            this.soTienGuiDataGridViewTextBoxColumn2.HeaderText = "Tổng số tiền gốc";
            this.soTienGuiDataGridViewTextBoxColumn2.Name = "soTienGuiDataGridViewTextBoxColumn2";
            this.soTienGuiDataGridViewTextBoxColumn2.Width = 200;
            // 
            // kyHanGuiDataGridViewTextBoxColumn2
            // 
            this.kyHanGuiDataGridViewTextBoxColumn2.DataPropertyName = "KyHanGui";
            this.kyHanGuiDataGridViewTextBoxColumn2.HeaderText = "Kỳ hạn gửi";
            this.kyHanGuiDataGridViewTextBoxColumn2.Name = "kyHanGuiDataGridViewTextBoxColumn2";
            this.kyHanGuiDataGridViewTextBoxColumn2.Width = 200;
            // 
            // laiSuatNamDataGridViewTextBoxColumn2
            // 
            this.laiSuatNamDataGridViewTextBoxColumn2.DataPropertyName = "LaiSuatNam";
            this.laiSuatNamDataGridViewTextBoxColumn2.HeaderText = "Lãi suất năm";
            this.laiSuatNamDataGridViewTextBoxColumn2.Name = "laiSuatNamDataGridViewTextBoxColumn2";
            this.laiSuatNamDataGridViewTextBoxColumn2.Width = 200;
            // 
            // btnXemSoTatToan
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.NullValue = "...";
            this.btnXemSoTatToan.DefaultCellStyle = dataGridViewCellStyle5;
            this.btnXemSoTatToan.HeaderText = "Xem";
            this.btnXemSoTatToan.Name = "btnXemSoTatToan";
            this.btnXemSoTatToan.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.btnXemSoTatToan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.btnXemSoTatToan.Text = "Xem";
            this.btnXemSoTatToan.UseColumnTextForButtonValue = true;
            // 
            // soTietKiemBindingSource1
            // 
            this.soTietKiemBindingSource1.DataMember = "SoTietKiem";
            this.soTietKiemBindingSource1.DataSource = this.qLSTKDataSetBindingSource;
            // 
            // qLSTKDataSetBindingSource
            // 
            this.qLSTKDataSetBindingSource.DataSource = this.qLSTKDataSet;
            this.qLSTKDataSetBindingSource.Position = 0;
            // 
            // qLSTKDataSet
            // 
            this.qLSTKDataSet.DataSetName = "QLSTKDataSet";
            this.qLSTKDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lblTatToan
            // 
            this.lblTatToan.AutoSize = true;
            this.lblTatToan.BackColor = System.Drawing.Color.Transparent;
            this.lblTatToan.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTatToan.ForeColor = System.Drawing.Color.Red;
            this.lblTatToan.Location = new System.Drawing.Point(11, 360);
            this.lblTatToan.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTatToan.Name = "lblTatToan";
            this.lblTatToan.Size = new System.Drawing.Size(157, 16);
            this.lblTatToan.TabIndex = 9;
            this.lblTatToan.Text = "Số lượng sổ đã tất toán";
            // 
            // lblTongTienNH
            // 
            this.lblTongTienNH.AutoSize = true;
            this.lblTongTienNH.BackColor = System.Drawing.Color.Transparent;
            this.lblTongTienNH.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTienNH.Location = new System.Drawing.Point(11, 85);
            this.lblTongTienNH.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTongTienNH.Name = "lblTongTienNH";
            this.lblTongTienNH.Size = new System.Drawing.Size(221, 16);
            this.lblTongTienNH.TabIndex = 6;
            this.lblTongTienNH.Text = "Tổng Tiền Theo Từng Ngân Hàng";
            // 
            // lblTongAllNganHang
            // 
            this.lblTongAllNganHang.AutoSize = true;
            this.lblTongAllNganHang.BackColor = System.Drawing.Color.Transparent;
            this.lblTongAllNganHang.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongAllNganHang.Location = new System.Drawing.Point(348, 26);
            this.lblTongAllNganHang.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTongAllNganHang.Name = "lblTongAllNganHang";
            this.lblTongAllNganHang.Size = new System.Drawing.Size(179, 16);
            this.lblTongAllNganHang.TabIndex = 5;
            this.lblTongAllNganHang.Text = "Tổng tiền tất cả ngân hàng";
            this.lblTongAllNganHang.Click += new System.EventHandler(this.lblTongAllNganHang_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(365, 4);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 22);
            this.label3.TabIndex = 4;
            this.label3.Text = "SỔ TIẾT KIỆM";
            // 
            // soTietKiemBindingSource12
            // 
            this.soTietKiemBindingSource12.DataMember = "SoTietKiem";
            this.soTietKiemBindingSource12.DataSource = this.qLSTKDataSet8;
            // 
            // qLSTKDataSet8
            // 
            this.qLSTKDataSet8.DataSetName = "QLSTKDataSet8";
            this.qLSTKDataSet8.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // soTietKiemBindingSource9
            // 
            this.soTietKiemBindingSource9.DataMember = "SoTietKiem";
            this.soTietKiemBindingSource9.DataSource = this.qLSTKDataSet5;
            // 
            // qLSTKDataSet5
            // 
            this.qLSTKDataSet5.DataSetName = "QLSTKDataSet5";
            this.qLSTKDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // soTietKiemBindingSource6
            // 
            this.soTietKiemBindingSource6.DataMember = "SoTietKiem";
            this.soTietKiemBindingSource6.DataSource = this.qLSTKDataSet2;
            // 
            // qLSTKDataSet2
            // 
            this.qLSTKDataSet2.DataSetName = "QLSTKDataSet2";
            this.qLSTKDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // soTietKiemBindingSource5
            // 
            this.soTietKiemBindingSource5.DataMember = "SoTietKiem";
            this.soTietKiemBindingSource5.DataSource = this.qLSTKDataSetBindingSource;
            // 
            // soTietKiemBindingSource4
            // 
            this.soTietKiemBindingSource4.DataMember = "SoTietKiem";
            this.soTietKiemBindingSource4.DataSource = this.qLSTKDataSet1;
            // 
            // qLSTKDataSet1
            // 
            this.qLSTKDataSet1.DataSetName = "QLSTKDataSet1";
            this.qLSTKDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // soTietKiemBindingSource
            // 
            this.soTietKiemBindingSource.DataMember = "SoTietKiem";
            this.soTietKiemBindingSource.DataSource = this.qLSTKDataSet;
            // 
            // soTietKiemTableAdapter
            // 
            this.soTietKiemTableAdapter.ClearBeforeFill = true;
            // 
            // soTietKiemTableAdapter1
            // 
            this.soTietKiemTableAdapter1.ClearBeforeFill = true;
            // 
            // soTietKiemTableAdapter2
            // 
            this.soTietKiemTableAdapter2.ClearBeforeFill = true;
            // 
            // qlstkDataSet3
            // 
            this.qlstkDataSet3.DataSetName = "QLSTKDataSet";
            this.qlstkDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // qlstkDataSet4
            // 
            this.qlstkDataSet4.DataSetName = "QLSTKDataSet";
            this.qlstkDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // qLSTKDataSet31
            // 
            this.qLSTKDataSet31.DataSetName = "QLSTKDataSet3";
            this.qLSTKDataSet31.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // soTietKiemBindingSource7
            // 
            this.soTietKiemBindingSource7.DataMember = "SoTietKiem";
            this.soTietKiemBindingSource7.DataSource = this.qLSTKDataSet31;
            // 
            // soTietKiemTableAdapter3
            // 
            this.soTietKiemTableAdapter3.ClearBeforeFill = true;
            // 
            // qLSTKDataSet41
            // 
            this.qLSTKDataSet41.DataSetName = "QLSTKDataSet4";
            this.qLSTKDataSet41.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // soTietKiemBindingSource8
            // 
            this.soTietKiemBindingSource8.DataMember = "SoTietKiem";
            this.soTietKiemBindingSource8.DataSource = this.qLSTKDataSet41;
            // 
            // soTietKiemTableAdapter4
            // 
            this.soTietKiemTableAdapter4.ClearBeforeFill = true;
            // 
            // soTietKiemTableAdapter5
            // 
            this.soTietKiemTableAdapter5.ClearBeforeFill = true;
            // 
            // qLSTKDataSet6
            // 
            this.qLSTKDataSet6.DataSetName = "QLSTKDataSet6";
            this.qLSTKDataSet6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // soTietKiemBindingSource10
            // 
            this.soTietKiemBindingSource10.DataMember = "SoTietKiem";
            this.soTietKiemBindingSource10.DataSource = this.qLSTKDataSet6;
            // 
            // soTietKiemTableAdapter6
            // 
            this.soTietKiemTableAdapter6.ClearBeforeFill = true;
            // 
            // qLSTKDataSet7
            // 
            this.qLSTKDataSet7.DataSetName = "QLSTKDataSet7";
            this.qLSTKDataSet7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // soTietKiemBindingSource11
            // 
            this.soTietKiemBindingSource11.DataMember = "SoTietKiem";
            this.soTietKiemBindingSource11.DataSource = this.qLSTKDataSet7;
            // 
            // soTietKiemTableAdapter7
            // 
            this.soTietKiemTableAdapter7.ClearBeforeFill = true;
            // 
            // soTietKiemTableAdapter8
            // 
            this.soTietKiemTableAdapter8.ClearBeforeFill = true;
            // 
            // qLSTKDataSet9
            // 
            this.qLSTKDataSet9.DataSetName = "QLSTKDataSet9";
            this.qLSTKDataSet9.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // soTietKiemBindingSource13
            // 
            this.soTietKiemBindingSource13.DataMember = "SoTietKiem";
            this.soTietKiemBindingSource13.DataSource = this.qLSTKDataSet9;
            // 
            // soTietKiemTableAdapter9
            // 
            this.soTietKiemTableAdapter9.ClearBeforeFill = true;
            // 
            // soTietKiemTableAdapter10
            // 
            this.soTietKiemTableAdapter10.ClearBeforeFill = true;
            // 
            // sotietkiemBindingSource2
            // 
            this.sotietkiemBindingSource2.DataSource = typeof(QLSTK.Sotietkiem);
            // 
            // sotietkiemBindingSource3
            // 
            this.sotietkiemBindingSource3.DataSource = typeof(QLSTK.Sotietkiem);
            // 
            // FormSoTietKiem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 625);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "FormSoTietKiem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sổ tiết kiệm";
            this.Load += new System.EventHandler(this.FormSoTietKiem_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvnganhang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTatToan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qlstkDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qlstkDataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qLSTKDataSet9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soTietKiemBindingSource13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sotietkiemBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sotietkiemBindingSource3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvTatToan;
        private System.Windows.Forms.Label lblTatToan;
        private System.Windows.Forms.Label lblTongTienNH;
        private System.Windows.Forms.Label lblTongAllNganHang;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn mãSốDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn tổngSốTiềnGốcDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kỳHạnGửiDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn lãiSuấtNămDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn chỉnhSửaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn mãSốDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn tổngSốTiềnGốcDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn kỳHạnGửiDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn lãiSuấtNămDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn xemDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mãSốDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tổngSốTiềnGốcDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kỳHạnGửiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lãiSuấtNămDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xemDataGridViewTextBoxColumn1;
        private QLSTKDataSet qLSTKDataSet;
        private System.Windows.Forms.BindingSource soTietKiemBindingSource;
        private QLSTKDataSetTableAdapters.SoTietKiemTableAdapter soTietKiemTableAdapter;
        private System.Windows.Forms.BindingSource qLSTKDataSetBindingSource;
        private System.Windows.Forms.Label lbdemso;
        private System.Windows.Forms.BindingSource soTietKiemBindingSource1;
        private System.Windows.Forms.BindingSource sotietkiemBindingSource2;
        private System.Windows.Forms.BindingSource sotietkiemBindingSource3;
        private QLSTKDataSet1 qLSTKDataSet1;
        private System.Windows.Forms.BindingSource soTietKiemBindingSource4;
        private QLSTKDataSet1TableAdapters.SoTietKiemTableAdapter soTietKiemTableAdapter1;
        private System.Windows.Forms.BindingSource soTietKiemBindingSource5;
        private QLSTKDataSet2 qLSTKDataSet2;
        private System.Windows.Forms.BindingSource soTietKiemBindingSource6;
        private QLSTKDataSet2TableAdapters.SoTietKiemTableAdapter soTietKiemTableAdapter2;
        private QLSTKDataSet qlstkDataSet3;
        private QLSTKDataSet qlstkDataSet4;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Label lblWelcome;
        private QLSTKDataSet3 qLSTKDataSet31;
        private System.Windows.Forms.BindingSource soTietKiemBindingSource7;
        private QLSTKDataSet3TableAdapters.SoTietKiemTableAdapter soTietKiemTableAdapter3;
        private System.Windows.Forms.ComboBox cBNganHang;
        private QLSTKDataSet4 qLSTKDataSet41;
        private System.Windows.Forms.BindingSource soTietKiemBindingSource8;
        private QLSTKDataSet4TableAdapters.SoTietKiemTableAdapter soTietKiemTableAdapter4;
        private QLSTKDataSet5 qLSTKDataSet5;
        private System.Windows.Forms.BindingSource soTietKiemBindingSource9;
        private QLSTKDataSet5TableAdapters.SoTietKiemTableAdapter soTietKiemTableAdapter5;
        private QLSTKDataSet6 qLSTKDataSet6;
        private System.Windows.Forms.BindingSource soTietKiemBindingSource10;
        private QLSTKDataSet6TableAdapters.SoTietKiemTableAdapter soTietKiemTableAdapter6;
        private QLSTKDataSet7 qLSTKDataSet7;
        private System.Windows.Forms.BindingSource soTietKiemBindingSource11;
        private QLSTKDataSet7TableAdapters.SoTietKiemTableAdapter soTietKiemTableAdapter7;
        private QLSTKDataSet8 qLSTKDataSet8;
        private System.Windows.Forms.BindingSource soTietKiemBindingSource12;
        private QLSTKDataSet8TableAdapters.SoTietKiemTableAdapter soTietKiemTableAdapter8;
        private QLSTKDataSet9 qLSTKDataSet9;
        private System.Windows.Forms.BindingSource soTietKiemBindingSource13;
        private QLSTKDataSet9TableAdapters.SoTietKiemTableAdapter soTietKiemTableAdapter9;
        private System.Windows.Forms.DataGridView dgvnganhang;
        private QLSTKDataSet10 qLSTKDataSet10;
        private System.Windows.Forms.BindingSource soTietKiemBindingSource14;
        private QLSTKDataSet10TableAdapters.SoTietKiemTableAdapter soTietKiemTableAdapter10;
        private System.Windows.Forms.Button btnSua;
        private System.Windows.Forms.Button btnGuiThem;
        private System.Windows.Forms.Button btnRutMotPhan;
        private System.Windows.Forms.Button btnTatToan;
        private System.Windows.Forms.Button btnDangXuat;
        private System.Windows.Forms.DataGridViewTextBoxColumn maSoTKDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn soTienGuiDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn kyHanGuiDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn laiSuatNamDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewButtonColumn btnXemSoTatToan;
        private System.Windows.Forms.DataGridViewTextBoxColumn maNHDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maSoTKDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ngayGuiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ngayHetHanDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn soTienGuiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kyHanGuiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn laiSuatNamDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn laiSuatKhongKyHanDataGridViewTextBoxColumn;
    }
}