﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTK
{
    public partial class FrmThemNganHang : Form
    {
        SqlConnection cnn = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=QLSTK;Integrated Security=True");
        public static string UserName = "";
        KetnoiCSDL kn;
        Sotietkiem stk = new Sotietkiem();
        public FrmThemNganHang()
        {
            InitializeComponent();
            kn = new KetnoiCSDL("HP-ELITEBOOK-87\\SQLEXPRESS", "QLSTK", true, "", "");
        }

        public bool KiemTraNhapNganHang(string ma, string ten)
        {
            if(string.IsNullOrEmpty(ten))
            {
                MessageBox.Show("Không được bỏ trống tên ngân hàng!");
                return false;
            }
            else if(string.IsNullOrEmpty(ma))
            {
                MessageBox.Show("Không được bỏ trống mã ngân hàng!");
                return false;
            }
            return true;
            
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            if(KiemTraNhapNganHang(txtMaNH.Text,txtTenNganHang.Text))
            {
                try
                {
                    stk.ThemNganHang(txtMaNH.Text, txtTenNganHang.Text);
                    MessageBox.Show("Đã Thêm Ngân Hàng: " + txtTenNganHang.Text + "(" + txtMaNH.Text + ")");
                    comboboxNH();
                }
                catch
                {
                    MessageBox.Show("Ngân hàng đã tồn tại!");
                }
            }
          
        }

        private void txtTenNganHang_TextChanged(object sender, EventArgs e)
        {
            string result = "";

            string input = txtTenNganHang.Text;

            string[] words = input.Split(' ');
            try
            {
                foreach (string word in words)

                {

                    result += word[0];

                }
            }
            catch
            {

            }
            
            txtMaNH.Text = result;
        }

        private void comboboxNH()
        {
            cnn.Open();
            string sql = "select MaNH from NganHang";
            SqlCommand command = new SqlCommand(sql, cnn); //bat dau truy van
            //command.Parameters.Add(new SqlParameter("@email", UserName));
            command.ExecuteScalar();
            SqlDataAdapter da = new SqlDataAdapter(command); //chuyen du lieu ve
            DataTable dt = new DataTable(); //tạo một kho ảo để lưu trữ dữ liệu
            da.Fill(dt);  // đổ dữ liệu vào kho
            cnn.Close();  // đóng kết nối
            cBNganHang1.DataSource = dt; //đổ dữ liệu vào cb
            cBNganHang1.DisplayMember = "MaNH";
            cBNganHang1.ValueMember = "MaNH";
        }
        private void FrmThemNganHang_Load(object sender, EventArgs e)
        {
            comboboxNH();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

        }

        private void btnXoaNH_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult rs = MessageBox.Show("Xóa Ngân Hàng: " + cBNganHang1.Text + "?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rs == DialogResult.Yes)
                {
                    stk.XoaNganHang(cBNganHang1.Text);
                    comboboxNH();
                }
            }
            catch
            {
                MessageBox.Show("Bạn đã xóa hết ngân hàng rồi! -_-");
            }
            
            
        }
    }
}
