﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTK
{
    public partial class FrmThemSoTietKiem : Form
    {
        String strConnection = "Data Source=.\\SQLEXPRESS;Initial Catalog=QLSTK;Integrated Security=True";
        SqlConnection cnn = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=QLSTK;Integrated Security=True");
        SqlConnection conn;
        SqlCommand command;
        public static string UserName = "";
        public FrmThemSoTietKiem()
        {
            InitializeComponent();
        }


        public FrmThemSoTietKiem(FormSoTietKiem fDS) 
        {
            
            InitializeComponent();


            txtLaiSuatKhongKyHan.Text = "0.05";
            cBKhiDenHan.Text = "Tái tục gốc và lãi";
            cBTraLai.Text = "Cuối kỳ";
            comboboxNH();
            string LaiSuatKoKyHan = cBKyHan.SelectedItem.ToString();
            if (LaiSuatKoKyHan.Equals("không kỳ hạn"))
            {
                txtLaiSuatKhongKyHan.Enabled = true;
                txtLaiSuat.Enabled = false;
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            Hide();
        }

       

        public void comboboxNH()
        {
            cnn.Open();
            string sql = "select MaNH from NganHang";
            SqlCommand command = new SqlCommand(sql, cnn); //bat dau truy van
            //command.Parameters.Add(new SqlParameter("@email", UserName));
            command.ExecuteScalar();
            SqlDataAdapter da = new SqlDataAdapter(command); //chuyen du lieu ve
            DataTable dt = new DataTable(); //tạo một kho ảo để lưu trữ dữ liệu
            da.Fill(dt);  // đổ dữ liệu vào kho
            cnn.Close();  // đóng kết nối
            cBNganHang1.DataSource = dt; //đổ dữ liệu vào cb
            cBNganHang1.DisplayMember = "MaNH";
            cBNganHang1.ValueMember = "MaNH";

        }

        public bool KiemTraNhapLaiSuat(string laisuat, string laisuatkokihan)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
           
            if(txtLaiSuatKhongKyHan.Enabled==true)
            {
                if (!regex.IsMatch(laisuatkokihan))
                {
                    MessageBox.Show("Lãi suất không kỳ hạn phải nhập bằng số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            else if (txtLaiSuat.Enabled == true)
            {
                if (!regex.IsMatch(laisuat))
                {
                    MessageBox.Show("Lãi suất phải nhập bằng số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            
            return true;

        }
        public bool KiemTraTxtChuaNhap(string _txtLaiSuat, string _txtSoTienGui)
        {
               
            if (string.IsNullOrEmpty(_txtSoTienGui))
            {
                MessageBox.Show("Không được để trống Số Tiền Gửi!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (txtLaiSuatKhongKyHan.Enabled == false)
            {
                if (string.IsNullOrEmpty(_txtLaiSuat))
                {
                    MessageBox.Show("Không được để trống Lãi Suất!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

            }
            return true;
            
        }
        public bool KiemTraNhapSoTienGui(string nhapsotiengui)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            if (regex.IsMatch(nhapsotiengui))
            {
                double sotiengui = double.Parse(nhapsotiengui);
                if (sotiengui > 1000000)
                {
                    return true;
                }

                else
                {
                    MessageBox.Show("Số tiền gửi tối thiểu là một triệu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            else
            {
                MessageBox.Show("Số tiền gửi phải nhập bằng số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
          return false;
        }
        public bool KiemTraNgayGui(string ngaygui)
        {
            DateTime dt = DateTime.Now;
            if (ngaygui.CompareTo(dt.ToShortDateString()) == 1)
            {
                MessageBox.Show("Ngày gửi phải nhỏ hoặc bằng hơn ngày hiện tại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

       
       
        public void XuLyBtnThem(string sotiengui)
        {
            string sql = "insert into SoTietKiem(NgayGui,KyHanGui,LaiSuatKhongKyHan,KhiDenHan,MaNH,SoTienGui,LaiSuatNam,TraLai,Email,TatToan) values(@ngaygui,@kyhan,@laisuatkokyhan,@khidenhan, @nganhang,@sotiengui,@laisuat,@tralai,@email,0)";
            //if (txtSoTienGui.Text.Length <= 6)
            //{
            //    MessageBox.Show("Số tiền gửi tối thiểu là 1 triệu đồng. Vui lòng kiểm tra lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            //}
            
            sotiengui = txtSoTienGui.Text;
            if (double.Parse(sotiengui) < 1000000)
            {
                MessageBox.Show("Số tiền gửi tối thiểu là một triệu !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                try
                {
                    conn = new SqlConnection(strConnection);
                    conn.Open();
                    command = new SqlCommand(sql, conn);
                    command.Parameters.Add(new SqlParameter("@ngaygui", dTNgayGui.Text));
                    command.Parameters.Add(new SqlParameter("@kyhan", cBKyHan.Text));
                    command.Parameters.Add(new SqlParameter("@laisuatkokyhan", txtLaiSuatKhongKyHan.Text));
                    command.Parameters.Add(new SqlParameter("@khidenhan", cBKhiDenHan.Text));
                    command.Parameters.Add(new SqlParameter("@nganhang", cBNganHang1.Text));
                    command.Parameters.Add(new SqlParameter("@sotiengui", txtSoTienGui.Text));
                    command.Parameters.Add(new SqlParameter("@laisuat", txtLaiSuat.Text));
                    command.Parameters.Add(new SqlParameter("@tralai", cBTraLai.Text));
                    command.Parameters.Add(new SqlParameter("@email", FormSoTietKiem.UserName));
                    command.ExecuteScalar();
                    MessageBox.Show("Thêm thành công!");

                    FormSoTietKiem frm = new FormSoTietKiem();
                    //frm.Refresh();
                    Hide();
                    frm.Show();
                }

                catch
                {
                    MessageBox.Show("Thêm thất bại!");
                    Hide();
                }


            }

        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            string sql = "insert into SoTietKiem(NgayGui,NgayHetHan,KyHanGui,LaiSuatKhongKyHan,KhiDenHan,MaNH,SoTienGui,LaiSuatNam,TraLai,Email,TatToan) values(@ngaygui,@ngayhethan,@kyhan,@laisuatkokyhan,@khidenhan, @nganhang,@sotiengui,@laisuat,@tralai,@email,0)";

            DateTime ngayhethan=DateTime.Now;
            if (cBKyHan.Text == ("1 tháng"))
            {
                ngayhethan = dTNgayGui.Value.AddMonths(1);
            }
            else if (cBKyHan.Text==("3 tháng"))
            {
                ngayhethan = dTNgayGui.Value.AddMonths(3);
            }
            else if (cBKyHan.Text == ("6 tháng"))
            {
                ngayhethan = dTNgayGui.Value.AddMonths(6);
            }
            else if (cBKyHan.Text == ("12 tháng"))
            {
                ngayhethan = dTNgayGui.Value.AddMonths(12);
            }

            else if (cBKyHan.Text == ("không kỳ hạn"))
            {
                ngayhethan = DateTime.MaxValue;
            }

            string _ngayhethan = ngayhethan.ToString();

            if (KiemTraNgayGui(dTNgayGui.Text)==true && KiemTraTxtChuaNhap(txtLaiSuat.Text, txtSoTienGui.Text) == true && KiemTraNhapLaiSuat(txtLaiSuat.Text, txtLaiSuatKhongKyHan.Text)==true && KiemTraNhapSoTienGui(txtSoTienGui.Text)==true)
            {

                try
                {
                    conn = new SqlConnection(strConnection);
                    conn.Open();
                    command = new SqlCommand(sql, conn);
                    command.Parameters.Add(new SqlParameter("@ngaygui", dTNgayGui.Text));
                    command.Parameters.Add(new SqlParameter("@ngayhethan", _ngayhethan));
                    command.Parameters.Add(new SqlParameter("@kyhan", cBKyHan.Text));
                    command.Parameters.Add(new SqlParameter("@laisuatkokyhan", txtLaiSuatKhongKyHan.Text));
                    command.Parameters.Add(new SqlParameter("@khidenhan", cBKhiDenHan.Text));
                    command.Parameters.Add(new SqlParameter("@nganhang", cBNganHang1.Text));
                    command.Parameters.Add(new SqlParameter("@sotiengui", txtSoTienGui.Text));
                    command.Parameters.Add(new SqlParameter("@laisuat", txtLaiSuat.Text));
                    command.Parameters.Add(new SqlParameter("@tralai", cBTraLai.Text));
                    command.Parameters.Add(new SqlParameter("@email", FormSoTietKiem.UserName));
                    command.ExecuteScalar();
                    MessageBox.Show("Thêm thành công!");


                    this.DialogResult = DialogResult.Cancel; //dong form
                }

                catch
                {
                    MessageBox.Show("Thêm thất bại!");
                    Hide();
                }
            }
                         
            
        }

        private void cBNganHang1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cBKyHan_SelectedIndexChanged(object sender, EventArgs e)
        {
            string LaiSuatKoKyHan = cBKyHan.SelectedItem.ToString();
            if (LaiSuatKoKyHan.Equals("không kỳ hạn"))
            {
                txtLaiSuatKhongKyHan.Text = "0.05";
                txtLaiSuatKhongKyHan.Enabled = true;
                txtLaiSuat.Enabled = false;
            }
            else
            {
                txtLaiSuatKhongKyHan.Enabled = false;
                txtLaiSuat.Enabled = true;
            }
        }

        private void txtSoTienGui_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void FrmThemSoTietKiem_Load(object sender, EventArgs e)
        {
            cBKyHan.Text = "không kỳ hạn";
            txtLaiSuatKhongKyHan.Text = "0.05";
            cBKhiDenHan.Text = "Tái tục gốc và lãi";
            cBTraLai.Text = "Cuối kỳ";
            comboboxNH();
            string LaiSuatKoKyHan = cBKyHan.SelectedItem.ToString();
            if (LaiSuatKoKyHan.Equals("không kỳ hạn"))
            {
                txtLaiSuatKhongKyHan.Enabled = true;
                txtLaiSuat.Enabled = false;
            }
        }

        private void btnThemNganHang_Click(object sender, EventArgs e)
        {
            FrmThemNganHang f = new FrmThemNganHang();
            if (f.ShowDialog() == DialogResult.OK)
            {

            }
            else
            {
                comboboxNH();
            }
        }
    }
}
