﻿namespace QLSTK
{
    partial class FormWelcome
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormWelcome));
            this.btnactive = new System.Windows.Forms.Button();
            this.btnInactive = new System.Windows.Forms.Button();
            this.lbten = new System.Windows.Forms.Label();
            this.panelgiaodien = new System.Windows.Forms.Panel();
            this.rd1 = new System.Windows.Forms.RadioButton();
            this.rd2 = new System.Windows.Forms.RadioButton();
            this.rd5 = new System.Windows.Forms.RadioButton();
            this.rd3 = new System.Windows.Forms.RadioButton();
            this.rd4 = new System.Windows.Forms.RadioButton();
            this.ptlogo = new System.Windows.Forms.PictureBox();
            this.panelgiaodien.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptlogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnactive
            // 
            this.btnactive.BackColor = System.Drawing.Color.White;
            this.btnactive.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnactive.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnactive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnactive.Location = new System.Drawing.Point(42, 367);
            this.btnactive.Margin = new System.Windows.Forms.Padding(2);
            this.btnactive.Name = "btnactive";
            this.btnactive.Size = new System.Drawing.Size(255, 40);
            this.btnactive.TabIndex = 0;
            this.btnactive.Text = "LẦN ĐẦU SỬ DỤNG MONEY LOVER";
            this.btnactive.UseVisualStyleBackColor = false;
            this.btnactive.Click += new System.EventHandler(this.btnactive_Click);
            // 
            // btnInactive
            // 
            this.btnInactive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnInactive.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnInactive.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInactive.ForeColor = System.Drawing.Color.White;
            this.btnInactive.Location = new System.Drawing.Point(42, 431);
            this.btnInactive.Margin = new System.Windows.Forms.Padding(2);
            this.btnInactive.Name = "btnInactive";
            this.btnInactive.Size = new System.Drawing.Size(259, 37);
            this.btnInactive.TabIndex = 1;
            this.btnInactive.Text = "ĐÃ SỬ DỤNG MONEY LOVER";
            this.btnInactive.UseVisualStyleBackColor = false;
            this.btnInactive.Click += new System.EventHandler(this.btnInactive_Click);
            // 
            // lbten
            // 
            this.lbten.AutoSize = true;
            this.lbten.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbten.ForeColor = System.Drawing.Color.White;
            this.lbten.Location = new System.Drawing.Point(39, 259);
            this.lbten.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbten.Name = "lbten";
            this.lbten.Size = new System.Drawing.Size(290, 32);
            this.lbten.TabIndex = 2;
            this.lbten.Text = "  Lên kế hoạch tài chính thông minh và từng \r\n   bước tiết kiệm để hiện thực hóa " +
    "ước mơ\r\n";
            // 
            // panelgiaodien
            // 
            this.panelgiaodien.BackColor = System.Drawing.Color.Transparent;
            this.panelgiaodien.Controls.Add(this.rd1);
            this.panelgiaodien.Controls.Add(this.rd2);
            this.panelgiaodien.Controls.Add(this.rd5);
            this.panelgiaodien.Controls.Add(this.rd3);
            this.panelgiaodien.Controls.Add(this.rd4);
            this.panelgiaodien.Controls.Add(this.ptlogo);
            this.panelgiaodien.Controls.Add(this.lbten);
            this.panelgiaodien.Controls.Add(this.btnInactive);
            this.panelgiaodien.Controls.Add(this.btnactive);
            this.panelgiaodien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelgiaodien.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.panelgiaodien.Location = new System.Drawing.Point(0, 0);
            this.panelgiaodien.Margin = new System.Windows.Forms.Padding(2);
            this.panelgiaodien.Name = "panelgiaodien";
            this.panelgiaodien.Size = new System.Drawing.Size(343, 496);
            this.panelgiaodien.TabIndex = 3;
            this.panelgiaodien.Paint += new System.Windows.Forms.PaintEventHandler(this.panelgiaodien_Paint);
            // 
            // rd1
            // 
            this.rd1.AutoSize = true;
            this.rd1.Checked = true;
            this.rd1.Location = new System.Drawing.Point(214, 322);
            this.rd1.Margin = new System.Windows.Forms.Padding(2);
            this.rd1.Name = "rd1";
            this.rd1.Size = new System.Drawing.Size(14, 13);
            this.rd1.TabIndex = 8;
            this.rd1.TabStop = true;
            this.rd1.UseVisualStyleBackColor = true;
            // 
            // rd2
            // 
            this.rd2.AutoSize = true;
            this.rd2.Location = new System.Drawing.Point(183, 322);
            this.rd2.Margin = new System.Windows.Forms.Padding(2);
            this.rd2.Name = "rd2";
            this.rd2.Size = new System.Drawing.Size(14, 13);
            this.rd2.TabIndex = 7;
            this.rd2.UseVisualStyleBackColor = true;
            // 
            // rd5
            // 
            this.rd5.AutoSize = true;
            this.rd5.Location = new System.Drawing.Point(98, 322);
            this.rd5.Margin = new System.Windows.Forms.Padding(2);
            this.rd5.Name = "rd5";
            this.rd5.Size = new System.Drawing.Size(14, 13);
            this.rd5.TabIndex = 6;
            this.rd5.UseVisualStyleBackColor = true;
            // 
            // rd3
            // 
            this.rd3.AutoSize = true;
            this.rd3.Location = new System.Drawing.Point(155, 322);
            this.rd3.Margin = new System.Windows.Forms.Padding(2);
            this.rd3.Name = "rd3";
            this.rd3.Size = new System.Drawing.Size(14, 13);
            this.rd3.TabIndex = 5;
            this.rd3.UseVisualStyleBackColor = true;
            // 
            // rd4
            // 
            this.rd4.AutoSize = true;
            this.rd4.Location = new System.Drawing.Point(128, 322);
            this.rd4.Margin = new System.Windows.Forms.Padding(2);
            this.rd4.Name = "rd4";
            this.rd4.Size = new System.Drawing.Size(14, 13);
            this.rd4.TabIndex = 4;
            this.rd4.UseVisualStyleBackColor = true;
            // 
            // ptlogo
            // 
            this.ptlogo.BackColor = System.Drawing.Color.Transparent;
            this.ptlogo.Image = ((System.Drawing.Image)(resources.GetObject("ptlogo.Image")));
            this.ptlogo.Location = new System.Drawing.Point(25, 17);
            this.ptlogo.Margin = new System.Windows.Forms.Padding(2);
            this.ptlogo.Name = "ptlogo";
            this.ptlogo.Size = new System.Drawing.Size(299, 221);
            this.ptlogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptlogo.TabIndex = 3;
            this.ptlogo.TabStop = false;
            this.ptlogo.Click += new System.EventHandler(this.ptlogo_Click);
            // 
            // FormWelcome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(343, 496);
            this.ControlBox = false;
            this.Controls.Add(this.panelgiaodien);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormWelcome";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panelgiaodien.ResumeLayout(false);
            this.panelgiaodien.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptlogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnactive;
        private System.Windows.Forms.Button btnInactive;
        private System.Windows.Forms.Label lbten;
        private System.Windows.Forms.Panel panelgiaodien;
        private System.Windows.Forms.PictureBox ptlogo;
        private System.Windows.Forms.RadioButton rd1;
        private System.Windows.Forms.RadioButton rd2;
        private System.Windows.Forms.RadioButton rd5;
        private System.Windows.Forms.RadioButton rd3;
        private System.Windows.Forms.RadioButton rd4;
    }
}
