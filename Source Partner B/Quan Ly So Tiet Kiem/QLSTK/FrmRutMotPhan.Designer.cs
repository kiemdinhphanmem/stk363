﻿namespace QLSTK
{
    partial class FrmRutMotPhan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNganHang = new System.Windows.Forms.Label();
            this.lblSoTienGoc = new System.Windows.Forms.Label();
            this.lblMaSo = new System.Windows.Forms.Label();
            this.btnRutTien = new System.Windows.Forms.Button();
            this.btnHuy = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoTienRut = new System.Windows.Forms.TextBox();
            this.lblNgayGui = new System.Windows.Forms.Label();
            this.lblNgayHetHan = new System.Windows.Forms.Label();
            this.lblKyHanGui = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblNganHang
            // 
            this.lblNganHang.AutoSize = true;
            this.lblNganHang.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblNganHang.Location = new System.Drawing.Point(38, 28);
            this.lblNganHang.Name = "lblNganHang";
            this.lblNganHang.Size = new System.Drawing.Size(76, 16);
            this.lblNganHang.TabIndex = 83;
            this.lblNganHang.Text = "Ngân Hàng:";
            // 
            // lblSoTienGoc
            // 
            this.lblSoTienGoc.AutoSize = true;
            this.lblSoTienGoc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblSoTienGoc.Location = new System.Drawing.Point(18, 54);
            this.lblSoTienGoc.Name = "lblSoTienGoc";
            this.lblSoTienGoc.Size = new System.Drawing.Size(96, 16);
            this.lblSoTienGoc.TabIndex = 82;
            this.lblSoTienGoc.Text = "Số tiền sẵn có:";
            // 
            // lblMaSo
            // 
            this.lblMaSo.AutoSize = true;
            this.lblMaSo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblMaSo.Location = new System.Drawing.Point(12, 5);
            this.lblMaSo.Name = "lblMaSo";
            this.lblMaSo.Size = new System.Drawing.Size(102, 16);
            this.lblMaSo.TabIndex = 81;
            this.lblMaSo.Text = "Mã sổ tiết kiệm:";
            // 
            // btnRutTien
            // 
            this.btnRutTien.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnRutTien.Location = new System.Drawing.Point(129, 222);
            this.btnRutTien.Name = "btnRutTien";
            this.btnRutTien.Size = new System.Drawing.Size(75, 23);
            this.btnRutTien.TabIndex = 79;
            this.btnRutTien.Text = "Rút tiền";
            this.btnRutTien.UseVisualStyleBackColor = true;
            this.btnRutTien.Click += new System.EventHandler(this.btnRutTien_Click);
            // 
            // btnHuy
            // 
            this.btnHuy.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnHuy.Location = new System.Drawing.Point(217, 222);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(75, 23);
            this.btnHuy.TabIndex = 78;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.UseVisualStyleBackColor = true;
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label3.Location = new System.Drawing.Point(14, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 16);
            this.label3.TabIndex = 77;
            this.label3.Text = "Nhập số tiền rút";
            // 
            // txtSoTienRut
            // 
            this.txtSoTienRut.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtSoTienRut.Location = new System.Drawing.Point(129, 174);
            this.txtSoTienRut.Name = "txtSoTienRut";
            this.txtSoTienRut.Size = new System.Drawing.Size(163, 22);
            this.txtSoTienRut.TabIndex = 76;
            // 
            // lblNgayGui
            // 
            this.lblNgayGui.AutoSize = true;
            this.lblNgayGui.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblNgayGui.Location = new System.Drawing.Point(49, 84);
            this.lblNgayGui.Name = "lblNgayGui";
            this.lblNgayGui.Size = new System.Drawing.Size(65, 16);
            this.lblNgayGui.TabIndex = 84;
            this.lblNgayGui.Text = "Ngày gửi:";
            // 
            // lblNgayHetHan
            // 
            this.lblNgayHetHan.AutoSize = true;
            this.lblNgayHetHan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblNgayHetHan.Location = new System.Drawing.Point(25, 111);
            this.lblNgayHetHan.Name = "lblNgayHetHan";
            this.lblNgayHetHan.Size = new System.Drawing.Size(89, 16);
            this.lblNgayHetHan.TabIndex = 85;
            this.lblNgayHetHan.Text = "Ngày hết hạn:";
            // 
            // lblKyHanGui
            // 
            this.lblKyHanGui.AutoSize = true;
            this.lblKyHanGui.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblKyHanGui.Location = new System.Drawing.Point(38, 138);
            this.lblKyHanGui.Name = "lblKyHanGui";
            this.lblKyHanGui.Size = new System.Drawing.Size(76, 16);
            this.lblKyHanGui.TabIndex = 86;
            this.lblKyHanGui.Text = "Kỳ hạn gửi:";
            // 
            // FrmRutMotPhan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 257);
            this.ControlBox = false;
            this.Controls.Add(this.lblKyHanGui);
            this.Controls.Add(this.lblNgayHetHan);
            this.Controls.Add(this.lblNgayGui);
            this.Controls.Add(this.lblNganHang);
            this.Controls.Add(this.lblSoTienGoc);
            this.Controls.Add(this.lblMaSo);
            this.Controls.Add(this.btnRutTien);
            this.Controls.Add(this.btnHuy);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtSoTienRut);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "FrmRutMotPhan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rút 1 Phần";
            this.Load += new System.EventHandler(this.FrmRutMotPhan_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNganHang;
        private System.Windows.Forms.Label lblSoTienGoc;
        private System.Windows.Forms.Label lblMaSo;
        private System.Windows.Forms.Button btnRutTien;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSoTienRut;
        private System.Windows.Forms.Label lblNgayGui;
        private System.Windows.Forms.Label lblNgayHetHan;
        private System.Windows.Forms.Label lblKyHanGui;
    }
}