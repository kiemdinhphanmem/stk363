﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Extensions.Forms;

namespace QLSTK
{
    [TestFixture]
    class Test_FrmSuaSoTietKiem
    {
        private FrmSuaSoTietKiem frm;
        private FormDangNhap frmDN;
        private FormSoTietKiem frmSTK;


        [SetUp]
        public void ChayFormDN()
        {
            frmDN = new FormDangNhap();
            frmDN.Show();
        }


        [SetUp]
        public void ChayFormSTK()
        {
            frmSTK = new FormSoTietKiem();
            frmSTK.Show();
        }

        [SetUp]
        public void ChayFormSSTK()
        {
            frm = new FrmSuaSoTietKiem();
            frm.Show();
        }

        [Test]
        public void TestKhoiTaoFormSuaSoTietKiem()
        {
            Assert.AreEqual("Sửa sổ tiết kiệm", frm.Text);
        }

       


        [Test]
        public void TestBtn_SSTK()
        {
            

            ButtonTester btncapnhat = new ButtonTester("btnCapNhat", frm);
            Assert.AreEqual("Cập nhật", btncapnhat.Text);

            ButtonTester btnHuy = new ButtonTester("btnHuy", frm);
            Assert.AreEqual("Hủy", btnHuy.Text);

        }


        [Test] // test so tien gui duoi 1 trieu
        public void Test_FrmSuaSTK_NhapSoTienGui_UTC1()
        {
            ChayFormSSTK();

            Assert.AreEqual(false, frm.KiemTraNhapSoTienGui(100000.ToString()), "Số tiền gửi tối thiểu là một triệu!");
        }

        [Test] // test so tien gui tren 1 trieu
        public void Test_FrmSuaSTK_NhapSoTienGui_UTC2()
        {
            Assert.AreEqual(true, frm.KiemTraNhapSoTienGui(2000000.ToString()));
        }

        [Test] // test so tien gui nhap chu va so
        public void Test_FrmSuaSTK_NhapSoTienGui_UTC3()
        {
            Assert.AreEqual(false, frm.KiemTraNhapSoTienGui("1000000abc"), "Số tiền gửi phải nhập bằng số!");
        }

        [Test] // test so tien gui nhap chu
        public void Test_FrmSuaSTK_NhapSoTienGui_UTC4()
        {
            Assert.AreEqual(false, frm.KiemTraNhapSoTienGui("abc"), "Số tiền gửi phải nhập bằng số!");
        }

        [Test] // test so tien gui bo trong
        public void Test_FrmSuaSTK_NhapSoTienGui_UTC5()
        {
            Assert.AreEqual(false, frm.KiemTraTxtChuaNhap("5", ""), "Không được để trống Số Tiền Gửi!");
        }



        [Test] // test lai suat nhap so
        public void Test_FrmSuaSTK_NhapLaiSuat_UTC1()
        {
            ComboBoxTester cbKyHan = new ComboBoxTester("cBKyHan", frm);
            if (cbKyHan.Text != "không kỳ hạn")
            {
                Assert.AreEqual(false, frm.KiemTraNhapLaiSuat("5abc", ""), "Lãi suất phải nhập bằng số!");
            }
        }

        [Test] // test lai suat/nam bo trong
        public void Test_FrmSuaSTK_NhapLaiSuat_UTC2()
        {
            ComboBoxTester cbKyHan = new ComboBoxTester("cBKyHan", frm);
            if (cbKyHan.Text != "không kỳ hạn")
            {
                Assert.AreEqual(true, frm.KiemTraTxtChuaNhap("", "2000000"), "Không được để trống Lãi Suất!");
            }
            

        }


        [Test] // test lai suat khong ky han nhap chu va so
        public void Test_FrmSuaSTK_NhapLaiSuatKhongKyHan_UTC1()
        {
            ComboBoxTester cbKyHan = new ComboBoxTester("cBKyHan", frm);
            if (cbKyHan.Text == "không kỳ hạn")
            {
                Assert.AreEqual(false, frm.KiemTraNhapLaiSuat("", "5abc"), "Lãi suất không kỳ hạn phải nhập bằng số!");
            }

        }

        [Test] // test lai suat khong ky han nhap chu
        public void Test_FrmSuaSTK_NhapLaiSuatKhongKyHan_UTC2()
        {
            ComboBoxTester cbKyHan = new ComboBoxTester("cBKyHan", frm);
            if (cbKyHan.Text == "không kỳ hạn")
            {
                Assert.AreEqual(false, frm.KiemTraNhapLaiSuat("", "abc"), "Lãi suất không kỳ hạn phải nhập bằng số!");
            }

        }




    }
}
