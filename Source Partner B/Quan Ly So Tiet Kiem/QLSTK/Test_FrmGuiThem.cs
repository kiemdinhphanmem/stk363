﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Extensions.Forms;
using NUnit.Framework;

namespace QLSTK
{
    [TestFixture]
    class Test_FrmGuiThem
    {
        private FrmGuiThem frm;


        [SetUp]
        public void ChayFormGuiThem()
        {
            frm = new FrmGuiThem();
            frm.Show();
        }

        [Test]
        public void TestKhoiTaoFormGuiThem()
        {
            Assert.AreEqual("Gửi Thêm", frm.Text);
        }

       


        [Test]
        public void TestBtn_GSTK()
        {
            ChayFormGuiThem();

            ButtonTester btnguithem = new ButtonTester("btnGuiThem", frm);
            Assert.AreEqual("Gửi thêm", btnguithem.Text);
            ButtonTester btnHuy = new ButtonTester("btnHuy", frm);
            Assert.AreEqual("Hủy", btnHuy.Text);

        }

        [Test] // test so tien gui nhap chu
        public void Test_FrmGuiThem_NhapSoTienGui_UTC1()
        {
            Assert.AreEqual(false, frm.KiemTraNhapSoTienGuiThem("abc"), "Số tiền gửi thêm phải nhập bằng số!");
        }

        [Test] // test so tien gui nhap chu va so
        public void Test_FrmGuiThem_NhapSoTienGui_UTC2()
        {
            Assert.AreEqual(false, frm.KiemTraNhapSoTienGuiThem("500000abc"), "Số tiền gửi thêm phải nhập bằng số!");
        }

        [Test] // test so tien gui them toi thieu la 100k
        public void Test_FrmGuiThem_NhapSoTienGui_UTC3()
        {
            Assert.AreEqual(false, frm.KiemTraNhapSoTienGuiThem("99000"), "Số tiền gửi thêm tối thiểu là một trăm nghìn(100000)!");
        }

        [Test] // test so tien gui them toi thieu la 100k
        public void Test_FrmGuiThem_NhapSoTienGui_UTC4()
        {
            Assert.AreEqual(false, frm.KiemTraNhapSoTienGuiThem(""), "Số tiền gửi thêm không được bỏ trống!");
        }



    }
}
