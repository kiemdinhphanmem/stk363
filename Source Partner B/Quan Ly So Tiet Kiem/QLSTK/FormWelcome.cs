﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTK
{
    public partial class FormWelcome : Form
    {
        public FormWelcome()
        {
            InitializeComponent();
        }

        private void pnlogo_Click(object sender, EventArgs e)
        {

        }

        private void btnactive_Click(object sender, EventArgs e)
        {
            FormDangKy frm = new FormDangKy();
            Hide();
            frm.Show();
        }

        private void ptlogo_Click(object sender, EventArgs e)
        {

        }

        private void btnInactive_Click(object sender, EventArgs e)
        {
            FormDangNhap frm = new FormDangNhap();
            Hide();
            frm.Show();
        }

        private void panelgiaodien_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
