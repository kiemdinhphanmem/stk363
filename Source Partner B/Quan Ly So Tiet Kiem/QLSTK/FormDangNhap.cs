﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QLSTK
{
    public partial class FormDangNhap : Form
    {
        String strConnection = "Data Source=.\\SQLEXPRESS;Initial Catalog=QLSTK;Integrated Security=True";
        SqlConnection conn;
        SqlCommand command;
        public static string UserName = "";
        public FormDangNhap()
        {
            InitializeComponent();
            

        }

        private void btndangnhap_Click(object sender, EventArgs e)
        {
            string sql = "select * from KhachHang where Email=@email and MatKhau=@matkhau";

            try
            {
                conn = new SqlConnection(strConnection);
                conn.Open();
                command = new SqlCommand(sql, conn);
                command.Parameters.Add(new SqlParameter("@email", txtemail.Text));
                command.Parameters.Add(new SqlParameter("@matKhau", txtmatkhau.Text));
                string rs = (string)command.ExecuteScalar();

                if (rs != null)
                {
                    MessageBox.Show("Đăng nhập thành công!");

                    FormSoTietKiem.UserName = txtemail.Text; // gán txt_email chào mừng để gắn vào lbl chào mừng bên frm SoTietKiem 
                    Hide();
                    FormSoTietKiem frm = new FormSoTietKiem();
                    frm.Show();
                }
                else
                {
                    MessageBox.Show("Sai tài khoản hoặc mật khẩu!");
                }
            }
            catch
            {

            }
            finally
            {
                conn.Close();
            }

        }

        private void FormDangNhap_Load(object sender, EventArgs e)
        {

        }

        private void btnDangKy_Click(object sender, EventArgs e)
        {
            Hide();
            FormDangKy frm = new FormDangKy();
            frm.Show();
        }
    }
}
