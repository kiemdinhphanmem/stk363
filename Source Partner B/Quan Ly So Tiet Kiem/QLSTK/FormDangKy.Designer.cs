﻿namespace QLSTK
{
    partial class FormDangKy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.paneldangky = new System.Windows.Forms.Panel();
            this.cbHienMatKhau = new System.Windows.Forms.CheckBox();
            this.txt_MatKhau = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_Email = new System.Windows.Forms.TextBox();
            this.btndangnhap = new System.Windows.Forms.Button();
            this.btndangky = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lbemail = new System.Windows.Forms.Label();
            this.paneldangky.SuspendLayout();
            this.SuspendLayout();
            // 
            // paneldangky
            // 
            this.paneldangky.BackColor = System.Drawing.Color.Gainsboro;
            this.paneldangky.Controls.Add(this.cbHienMatKhau);
            this.paneldangky.Controls.Add(this.txt_MatKhau);
            this.paneldangky.Controls.Add(this.label1);
            this.paneldangky.Controls.Add(this.txt_Email);
            this.paneldangky.Controls.Add(this.btndangnhap);
            this.paneldangky.Controls.Add(this.btndangky);
            this.paneldangky.Controls.Add(this.label2);
            this.paneldangky.Controls.Add(this.lbemail);
            this.paneldangky.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paneldangky.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paneldangky.ForeColor = System.Drawing.Color.Black;
            this.paneldangky.Location = new System.Drawing.Point(0, 0);
            this.paneldangky.Margin = new System.Windows.Forms.Padding(2);
            this.paneldangky.Name = "paneldangky";
            this.paneldangky.Size = new System.Drawing.Size(446, 294);
            this.paneldangky.TabIndex = 0;
            this.paneldangky.Paint += new System.Windows.Forms.PaintEventHandler(this.paneldangky_Paint);
            // 
            // cbHienMatKhau
            // 
            this.cbHienMatKhau.AutoSize = true;
            this.cbHienMatKhau.ForeColor = System.Drawing.Color.Blue;
            this.cbHienMatKhau.Location = new System.Drawing.Point(127, 116);
            this.cbHienMatKhau.Name = "cbHienMatKhau";
            this.cbHienMatKhau.Size = new System.Drawing.Size(117, 20);
            this.cbHienMatKhau.TabIndex = 8;
            this.cbHienMatKhau.Text = "Hiện mật khẩu";
            this.cbHienMatKhau.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.cbHienMatKhau.UseVisualStyleBackColor = true;
            this.cbHienMatKhau.CheckedChanged += new System.EventHandler(this.cbHienMatKhau_CheckedChanged);
            // 
            // txt_MatKhau
            // 
            this.txt_MatKhau.Location = new System.Drawing.Point(127, 88);
            this.txt_MatKhau.Margin = new System.Windows.Forms.Padding(2);
            this.txt_MatKhau.Name = "txt_MatKhau";
            this.txt_MatKhau.Size = new System.Drawing.Size(216, 23);
            this.txt_MatKhau.TabIndex = 7;
            this.txt_MatKhau.UseSystemPasswordChar = true;
            this.txt_MatKhau.TextChanged += new System.EventHandler(this.txt_MatKhau_TextChanged);
            this.txt_MatKhau.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_MatKhau_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 91);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Mật khẩu:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txt_Email
            // 
            this.txt_Email.Location = new System.Drawing.Point(127, 45);
            this.txt_Email.Margin = new System.Windows.Forms.Padding(2);
            this.txt_Email.Name = "txt_Email";
            this.txt_Email.Size = new System.Drawing.Size(216, 23);
            this.txt_Email.TabIndex = 5;
            this.txt_Email.TextChanged += new System.EventHandler(this.txt_Email_TextChanged);
            this.txt_Email.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_Email_KeyPress);
            // 
            // btndangnhap
            // 
            this.btndangnhap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btndangnhap.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndangnhap.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btndangnhap.Location = new System.Drawing.Point(171, 215);
            this.btndangnhap.Margin = new System.Windows.Forms.Padding(2);
            this.btndangnhap.Name = "btndangnhap";
            this.btndangnhap.Size = new System.Drawing.Size(129, 33);
            this.btndangnhap.TabIndex = 3;
            this.btndangnhap.Text = "ĐĂNG NHẬP";
            this.btndangnhap.UseVisualStyleBackColor = false;
            this.btndangnhap.Click += new System.EventHandler(this.btndangnhap_Click);
            // 
            // btndangky
            // 
            this.btndangky.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btndangky.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndangky.ForeColor = System.Drawing.Color.White;
            this.btndangky.Location = new System.Drawing.Point(171, 157);
            this.btndangky.Margin = new System.Windows.Forms.Padding(2);
            this.btndangky.Name = "btndangky";
            this.btndangky.Size = new System.Drawing.Size(129, 36);
            this.btndangky.TabIndex = 2;
            this.btndangky.Text = "ĐĂNG KÝ";
            this.btndangky.UseVisualStyleBackColor = false;
            this.btndangky.Click += new System.EventHandler(this.btndangky_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(67, 48);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Email:";
            // 
            // lbemail
            // 
            this.lbemail.AutoSize = true;
            this.lbemail.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbemail.Location = new System.Drawing.Point(123, 9);
            this.lbemail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbemail.Name = "lbemail";
            this.lbemail.Size = new System.Drawing.Size(177, 22);
            this.lbemail.TabIndex = 0;
            this.lbemail.Text = "Đăng ký bằng Email";
            // 
            // FormDangKy
            // 
            this.AcceptButton = this.btndangky;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 294);
            this.Controls.Add(this.paneldangky);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "FormDangKy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng ký";
            this.Load += new System.EventHandler(this.FormDangKy_Load);
            this.paneldangky.ResumeLayout(false);
            this.paneldangky.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel paneldangky;
        private System.Windows.Forms.Button btndangky;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbemail;
        private System.Windows.Forms.Button btndangnhap;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_Email;
        private System.Windows.Forms.TextBox txt_MatKhau;
        private System.Windows.Forms.CheckBox cbHienMatKhau;
    }
}