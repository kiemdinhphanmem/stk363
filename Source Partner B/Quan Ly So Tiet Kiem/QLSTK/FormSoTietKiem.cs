﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTK
{
    public partial class FormSoTietKiem : Form
    {
        

        public static string MaSoTK;
        public static double sotiengui;
        public static string KyHanGui;
        public static double LaiSuatNam;
        public static string NgayHetHan;
        public static DateTime NgaySuDung;
        public static double SoTienDu;

        int donghientai;

        SqlConnection cnn = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=QLSTK;Integrated Security=True");
        public static string UserName = "";
        SqlConnection kn;
        Sotietkiem stk = new Sotietkiem();
        public FormSoTietKiem()
        {
            InitializeComponent();
            kn = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=QLSTK;Integrated Security=True");
        }

        private void dataSoTK()
        {
            try
            {
                cnn.Open();
                string sql = "select MaNH, MaSoTK,NgayGui,SoTienGui,KyHanGui,LaiSuatNam,LaiSuatKhongKyHan,NgayGui from SoTietKiem where Email=@email order by MaNH";
                //TheoNganHang = cBNganHang.SelectedItem.ToString();

                SqlCommand command = new SqlCommand(sql, cnn); //bat dau truy van
                command.Parameters.Add(new SqlParameter("@email", UserName));
                command.ExecuteScalar();
                SqlDataAdapter da = new SqlDataAdapter(command); //chuyen du lieu ve
                DataTable dt = new DataTable(); //tạo một kho ảo để lưu trữ dữ liệu
                da.Fill(dt);  // đổ dữ liệu vào kho
                cnn.Close();  // đóng kết nối
                dgvnganhang.DataSource = dt; //đổ dữ liệu vào datagridview

            }
            catch
            { }
            finally
            {
                cnn.Close();
            }



        }

        private void comboboxNH()
        {
            cnn.Open();
            string sql = "select MaNH from NganHang";
            SqlCommand command = new SqlCommand(sql, cnn); //bat dau truy van
            //command.Parameters.Add(new SqlParameter("@email", UserName));
            command.ExecuteScalar();
            SqlDataAdapter da = new SqlDataAdapter(command); //chuyen du lieu ve
            DataTable dt = new DataTable(); //tạo một kho ảo để lưu trữ dữ liệu
            da.Fill(dt);  // đổ dữ liệu vào kho
            cnn.Close();  // đóng kết nối
            cBNganHang.DataSource = dt; //đổ dữ liệu vào cb
            cBNganHang.DisplayMember = "MaNH";
            cBNganHang.ValueMember = "MaNH";
        }
        public void FormSoTietKiem_Load(object sender, EventArgs e)
        {
          
            lblWelcome.Text = "Xin chào " + UserName; //chào mừng username gán bên đăng ký hoặc đăng nhập
            
            string TongTien = stk.GetTongTienByEmail(UserName).Rows[0][0].ToString(); //tong tien tat ca ngan hang theo email dang dang nhap
            
                lblTongAllNganHang.Text = "Tổng tiền trong sổ: " + TongTien + " VNĐ";
            
            

            string TongTienNH = stk.GetTongTienTheoNH(cBNganHang.Text, UserName).Rows[0][0].ToString(); //tong tien theo ngan hang chon trong combobox
            lblTongTienNH.Text = "Ngân Hàng " + cBNganHang.Text + ": " + TongTienNH + " VNĐ";

            string DemSoTatToan = stk.GetDemSoTatToan(UserName).Rows[0][0].ToString(); //tong tien theo ngan hang chon trong combobox
            lblTatToan.Text = "Đã tất toán: (" + DemSoTatToan+ " sổ)";


            try
            {
                DataTable dataCB = stk.LaySTK();
                //DataRow row = dataCB.NewRow();
                //row[0] = "Tất cả";
                //stk.LaySTK().Rows.Add(row);
                cBNganHang.DataSource = dataCB;
                cBNganHang.DisplayMember = "MaNH";
                cBNganHang.ValueMember = "MaNH";

                //if (GiaTriCBNganHang.Equals("Tất cả"))
                //{
                //    dgvnganhang.DataSource = stk.DanhsachSotietkiem(UserName);
                //}
                //else
                //{
                    dgvnganhang.DataSource = stk.DanhsachSotietkiemTheoNganHang(cBNganHang.Text, UserName);

                    dgvTatToan.DataSource = stk.GetDanhsachDaTatToan(UserName);
                //}

            }
            catch
            {
                MessageBox.Show("Lỗi hiển thị", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

     
        private void btnThem_Click(object sender, EventArgs e)
        {
            FrmThemSoTietKiem frm = new FrmThemSoTietKiem();
            if (frm.ShowDialog() == DialogResult.OK)
            {

            }
            else
            {
                FormSoTietKiem_Load(sender, e);
            }
            
        }

      

        private void lblWelcome_Click(object sender, EventArgs e)
        {

        }

        private void dgvnganhang_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            donghientai = dgvnganhang.CurrentRow.Index;
        }

        public void DuaDataTuGridViewVaoTxtUpdate()
        {

            DataGridViewRow datarow = dgvnganhang.Rows[dgvnganhang.CurrentRow.Index];
            FrmSuaSoTietKiem.NganHang = datarow.Cells[0].Value.ToString();
            FrmSuaSoTietKiem.MaSo = datarow.Cells[1].Value.ToString();
            FrmSuaSoTietKiem.NgayGui = DateTime.Parse(datarow.Cells[2].Value.ToString());
            FrmSuaSoTietKiem.SoTienGui = datarow.Cells[4].Value.ToString();
            FrmSuaSoTietKiem.KyHan = datarow.Cells[5].Value.ToString();
            FrmSuaSoTietKiem.LaiSuat = datarow.Cells[6].Value.ToString();
            FrmSuaSoTietKiem.LaiSuatKhongKyHan= datarow.Cells[7].Value.ToString();

            string id= datarow.Cells[1].Value.ToString();

            DateTime _ngaygui = DateTime.Parse(stk.GetDuLieuGanVaoUpdate(id).Rows[0][0].ToString());
            FrmSuaSoTietKiem.NgayGui = _ngaygui;

            string _khidenhan=stk.GetDuLieuGanVaoUpdate(id).Rows[0][1].ToString();
            FrmSuaSoTietKiem.KhiDenHan = _khidenhan;

            string _tralai = stk.GetDuLieuGanVaoUpdate(id).Rows[0][2].ToString();
            FrmSuaSoTietKiem.TraLai = _tralai;



        }
        private void cBNganHang_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvnganhang.DataSource = stk.DanhsachSotietkiemTheoNganHang(cBNganHang.SelectedValue.ToString(),UserName);

            string TongTienNH = stk.GetTongTienTheoNH(cBNganHang.Text, UserName).Rows[0][0].ToString(); //tong tien theo ngan hang chon trong combobox
            lblTongTienNH.Text = "Ngân Hàng " + cBNganHang.Text + ": " + TongTienNH + " VNĐ";
        }

        private void dgdatattoan_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridViewRow datarow = dgvTatToan.Rows[dgvTatToan.CurrentRow.Index];
                string idSo = datarow.Cells[0].Value.ToString();
                string layKyHan = datarow.Cells[2].Value.ToString();
                string nganhang_tt = stk.GetThongTinSoTatToan(idSo).Rows[0][1].ToString();
                string ngaygui_tt = stk.GetThongTinSoTatToan(idSo).Rows[0][2].ToString();
                string sotiengui_tt = stk.GetThongTinSoTatToan(idSo).Rows[0][4].ToString();
                string kyhangui_tt = stk.GetThongTinSoTatToan(idSo).Rows[0][5].ToString();
                string laisuatnam_tt = stk.GetThongTinSoTatToan(idSo).Rows[0][6].ToString();
                string laisuatkokyhan_tt = stk.GetThongTinSoTatToan(idSo).Rows[0][7].ToString();
                string khidenhan_tt = stk.GetThongTinSoTatToan(idSo).Rows[0][8].ToString();
                string tralai_tt = stk.GetThongTinSoTatToan(idSo).Rows[0][9].ToString();


                if (dgvTatToan.Columns[e.ColumnIndex].Name == "btnXemSoTatToan")
                {

                    if (layKyHan == "không kỳ hạn")
                    {
                        try
                        {
                            MessageBox.Show("Thông tin sổ tiết kiệm: " + nganhang_tt + "_" + idSo + "\n Ngày gửi: " + ngaygui_tt + "\n Số tiền gửi: " + sotiengui_tt + "\n Kỳ Hạn: " + kyhangui_tt + "\n Lãi suất: " + laisuatkokyhan_tt + "%/năm \n Khi đến hạn: " + khidenhan_tt + "\n Trả lãi: " + tralai_tt);
                        }
                        catch { }

                    }
                    else
                    {
                        try
                        {
                            MessageBox.Show("Thông tin sổ tiết kiệm: " + nganhang_tt + "_" + idSo + "\n Ngày gửi: " + ngaygui_tt + "\n Số tiền gửi: " + sotiengui_tt + "\n Kỳ Hạn: " + kyhangui_tt + "\n Lãi suất: " + laisuatnam_tt + "%/năm \n Khi đến hạn: " + khidenhan_tt + "\n Trả lãi: " + tralai_tt);
                        }
                        catch { }

                    }



                }
            }
            catch { }
            
            
            
            
        }

        private void lblWelcome_Click_1(object sender, EventArgs e)
        {

        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            


            try
            {
                DuaDataTuGridViewVaoTxtUpdate();
                FrmSuaSoTietKiem frm = new FrmSuaSoTietKiem();
                if (frm.ShowDialog() == DialogResult.OK)
                {

                }
                else
                {
                    FormSoTietKiem_Load(sender, e);
                }
            }
            catch
            {
                MessageBox.Show("Chưa có sổ tiết kiệm nào được chọn!");
            }
           
        }

        private void btnGuiThem_Click(object sender, EventArgs e)
        {
            try
            {
                FrmGuiThem frm = new FrmGuiThem();
                DataGridViewRow datarow = dgvnganhang.Rows[dgvnganhang.CurrentRow.Index];
                frm.TenNganHang = datarow.Cells[0].Value.ToString();
                frm.MaSoTK = datarow.Cells[1].Value.ToString();
                frm.Tongsotiengoc = double.Parse(datarow.Cells[4].Value.ToString());
               
                if (frm.ShowDialog() == DialogResult.OK)
                {

                }
                else
                {
                    FormSoTietKiem_Load(sender, e);
                }

                
            }
            catch
            {
                MessageBox.Show("Chưa có sổ tiết kiệm nào được chọn!");
            }
           
        }

        private void btnRutMotPhan_Click(object sender, EventArgs e)
        {
            try
            {
                FrmRutMotPhan frm = new FrmRutMotPhan();
                DataGridViewRow datarow = dgvnganhang.Rows[dgvnganhang.CurrentRow.Index];
                frm.TenNganHang = datarow.Cells[0].Value.ToString();
                frm.MaSoTK = datarow.Cells[1].Value.ToString();
                frm.NgayGuiTien = DateTime.Parse(datarow.Cells[2].Value.ToString());
                frm.NgayHetHan = DateTime.Parse(datarow.Cells[3].Value.ToString());
                frm.Tongsotiengoc = double.Parse(datarow.Cells[4].Value.ToString());
                frm.KyHanGui = datarow.Cells[5].Value.ToString();

                if (frm.ShowDialog() == DialogResult.OK) //show diaglog
                {

                }
                else
                {
                    FormSoTietKiem_Load(sender, e);
                }
            }
            catch
            {
                MessageBox.Show("Chưa có sổ tiết kiệm nào được chọn!");
            }


        }

        private void btnTatToan_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow datarow = dgvnganhang.Rows[dgvnganhang.CurrentRow.Index];
                string nganhang = datarow.Cells[0].Value.ToString(); //lay ngan hang so tiet kiem
                string maso = datarow.Cells[1].Value.ToString(); //lay id so tiet kiem
                string sotiengui = datarow.Cells[4].Value.ToString(); //lay so tien gui
                string kyhangui = datarow.Cells[5].Value.ToString(); //lay ky han gui
                string laisuatnam = datarow.Cells[6].Value.ToString(); //lay lai suat
                string laisuatkokyhan = datarow.Cells[7].Value.ToString(); //lay lai suat ko ky han
                DateTime _ngayGuiTien = DateTime.Parse(datarow.Cells[2].Value.ToString()); //lay ngay gui tien

                TimeSpan songaygui_time = DateTime.Now - _ngayGuiTien; //tinh tong so ngay gui tien bang cong thuc ngayhethan-ngayhientai
                string songaygui_hientai = Convert.ToString(songaygui_time.TotalDays); //lay tong so ngay gui tien ra string


                if (kyhangui == "không kỳ hạn") //nếu là sổ tiết kiệm ko kỳ hạn thì tính theo công thưc riêng
                {
                    DialogResult re = MessageBox.Show("Tất toán sổ tiết kiệm:\nNgân hàng: " + nganhang + "\nMã sổ: " + maso + "\nKỳ hạn gửi: " + kyhangui + "\nSố tiền gửi: " + sotiengui + "\nLãi suất: " + laisuatkokyhan + "%/năm", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);


                    if (re == DialogResult.Yes)
                    {
                        stk.TatToanStkKhongKyHan(songaygui_hientai, maso);

                        dgvnganhang.DataSource = stk.DanhsachSotietkiemTheoNganHang(cBNganHang.Text, UserName);
                        dgvTatToan.DataSource = stk.GetDanhsachDaTatToan(UserName);

                        string TongTien = stk.GetTongTienByEmail(UserName).Rows[0][0].ToString(); //tong tien tat ca ngan hang theo email dang dang nhap
                        lblTongAllNganHang.Text = "Tổng tiền trong sổ: " + TongTien + " VNĐ";

                        string TongTienNH = stk.GetTongTienTheoNH(cBNganHang.Text, UserName).Rows[0][0].ToString(); //tong tien theo ngan hang chon trong combobox
                        lblTongTienNH.Text = "Ngân Hàng " + cBNganHang.Text + ": " + TongTienNH + " VNĐ";

                        string DemSoTatToan = stk.GetDemSoTatToan(UserName).Rows[0][0].ToString(); //tong tien theo ngan hang chon trong combobox
                        lblTatToan.Text = "Đã tất toán: (" + DemSoTatToan + " sổ)";
                    }
                }
                else //ngược lại nếu có kỳ hạn
                {
                    DateTime _ngayHetHan = DateTime.Parse(datarow.Cells[3].Value.ToString()); //lay ngay het han
                    DialogResult rs = MessageBox.Show("Tất toán sổ tiết kiệm:\nNgân hàng: " + nganhang + "\nMã sổ: " + maso + "\nKỳ hạn gửi: " + kyhangui + "\nSố tiền gửi: " + sotiengui + "\nLãi suất: " + laisuatnam + "%/năm", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);


                    if (rs == DialogResult.Yes)
                    {

                        if (DateTime.Now < _ngayHetHan) //nếu tất toán trc ngày hết hạn
                        {
                            DialogResult rs1 = MessageBox.Show("Bạn đang tất toán sổ " + nganhang + "_" + maso + " trước kỳ hạn(" + _ngayHetHan + "), lãi suất sẽ được tính theo lãi suất không kỳ hạn là 0.05%/năm, bạn có muốn tiếp tục không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (rs1 == DialogResult.Yes)
                            {


                                stk.TatToanTruocKyHan(maso);
                                dgvnganhang.DataSource = stk.DanhsachSotietkiemTheoNganHang(cBNganHang.Text, UserName);
                                dgvTatToan.DataSource = stk.GetDanhsachDaTatToan(UserName);

                                string TongTien = stk.GetTongTienByEmail(UserName).Rows[0][0].ToString(); //tong tien tat ca ngan hang theo email dang dang nhap
                                lblTongAllNganHang.Text = "Tổng tiền trong sổ: " + TongTien + " VNĐ";

                                string TongTienNH = stk.GetTongTienTheoNH(cBNganHang.Text, UserName).Rows[0][0].ToString(); //tong tien theo ngan hang chon trong combobox
                                lblTongTienNH.Text = "Ngân Hàng " + cBNganHang.Text + ": " + TongTienNH + " VNĐ";

                                string DemSoTatToan = stk.GetDemSoTatToan(UserName).Rows[0][0].ToString(); //tong tien theo ngan hang chon trong combobox
                                lblTatToan.Text = "Đã tất toán: (" + DemSoTatToan + " sổ)";
                            }


                        }
                        else //ngược lại, tất toán từ ngày hết hạn trở đi
                        {

                            stk.TatToanSauKyHan(maso);
                            dgvnganhang.DataSource = stk.DanhsachSotietkiemTheoNganHang(cBNganHang.Text, UserName);
                            dgvTatToan.DataSource = stk.GetDanhsachDaTatToan(UserName);

                            string TongTien = stk.GetTongTienByEmail(UserName).Rows[0][0].ToString(); //tong tien tat ca ngan hang theo email dang dang nhap
                            lblTongAllNganHang.Text = "Tổng tiền trong sổ: " + TongTien + " VNĐ";

                            string TongTienNH = stk.GetTongTienTheoNH(cBNganHang.Text, UserName).Rows[0][0].ToString(); //tong tien theo ngan hang chon trong combobox
                            lblTongTienNH.Text = "Ngân Hàng " + cBNganHang.Text + ": " + TongTienNH + " VNĐ";

                            string DemSoTatToan = stk.GetDemSoTatToan(UserName).Rows[0][0].ToString(); //tong tien theo ngan hang chon trong combobox
                            lblTatToan.Text = "Đã tất toán: (" + DemSoTatToan + " sổ)";

                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Chưa có sổ tiết kiệm nào được chọn!");
            }
            

                

            
        }

        private void lbvc_Click(object sender, EventArgs e)
        {

        }

        private void btnDangXuat_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Bạn chắc chắn muốn đăng xuất?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (rs == DialogResult.Yes)
            {

                FormWelcome frm = new FormWelcome();
                Hide();
                frm.Show();
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblTongAllNganHang_Click(object sender, EventArgs e)
        {

        }
    }
}
