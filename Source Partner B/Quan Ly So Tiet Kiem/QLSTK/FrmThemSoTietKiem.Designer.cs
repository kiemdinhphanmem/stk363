﻿namespace QLSTK
{
    partial class FrmThemSoTietKiem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmThemSoTietKiem));
            this.label1 = new System.Windows.Forms.Label();
            this.cBNganHang1 = new System.Windows.Forms.ComboBox();
            this.btnThem = new System.Windows.Forms.Button();
            this.btnHuy = new System.Windows.Forms.Button();
            this.cBKyHan = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cBTraLai = new System.Windows.Forms.ComboBox();
            this.cBKhiDenHan = new System.Windows.Forms.ComboBox();
            this.txtLaiSuat = new System.Windows.Forms.TextBox();
            this.txtLaiSuatKhongKyHan = new System.Windows.Forms.TextBox();
            this.txtSoTienGui = new System.Windows.Forms.TextBox();
            this.dTNgayGui = new System.Windows.Forms.DateTimePicker();
            this.btnThemNganHang = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(13, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(439, 16);
            this.label1.TabIndex = 45;
            this.label1.Text = "Nếu không nhập lãi suất không kỳ hạn, thì mặc định lãi suất là 0.05%/năm";
            // 
            // cBNganHang1
            // 
            this.cBNganHang1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBNganHang1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cBNganHang1.FormattingEnabled = true;
            this.cBNganHang1.Location = new System.Drawing.Point(426, 10);
            this.cBNganHang1.Name = "cBNganHang1";
            this.cBNganHang1.Size = new System.Drawing.Size(130, 24);
            this.cBNganHang1.TabIndex = 44;
            this.cBNganHang1.SelectedIndexChanged += new System.EventHandler(this.cBNganHang1_SelectedIndexChanged);
            // 
            // btnThem
            // 
            this.btnThem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnThem.Location = new System.Drawing.Point(241, 140);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(75, 23);
            this.btnThem.TabIndex = 43;
            this.btnThem.Text = "Thêm";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnHuy
            // 
            this.btnHuy.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnHuy.Location = new System.Drawing.Point(366, 140);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(75, 23);
            this.btnHuy.TabIndex = 42;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.UseVisualStyleBackColor = true;
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // cBKyHan
            // 
            this.cBKyHan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBKyHan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cBKyHan.FormattingEnabled = true;
            this.cBKyHan.Items.AddRange(new object[] {
            "không kỳ hạn",
            "1 tháng",
            "3 tháng",
            "6 tháng",
            "12 tháng"});
            this.cBKyHan.Location = new System.Drawing.Point(153, 36);
            this.cBKyHan.Name = "cBKyHan";
            this.cBKyHan.Size = new System.Drawing.Size(163, 24);
            this.cBKyHan.TabIndex = 41;
            this.cBKyHan.SelectedIndexChanged += new System.EventHandler(this.cBKyHan_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label9.Location = new System.Drawing.Point(341, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 16);
            this.label9.TabIndex = 40;
            this.label9.Text = "Ngân hàng";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label8.Location = new System.Drawing.Point(13, 67);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 16);
            this.label8.TabIndex = 39;
            this.label8.Text = "Lãi suất không kỳ hạn";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label7.Location = new System.Drawing.Point(85, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 16);
            this.label7.TabIndex = 38;
            this.label7.Text = "Kỳ hạn";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label6.Location = new System.Drawing.Point(76, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 16);
            this.label6.TabIndex = 37;
            this.label6.Text = "Ngày gửi";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label5.Location = new System.Drawing.Point(362, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 16);
            this.label5.TabIndex = 36;
            this.label5.Text = "Trả lãi";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label4.Location = new System.Drawing.Point(357, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 16);
            this.label4.TabIndex = 35;
            this.label4.Text = "Lãi suất";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label3.Location = new System.Drawing.Point(341, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 34;
            this.label3.Text = "Số tiền gửi";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.Location = new System.Drawing.Point(60, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 16);
            this.label2.TabIndex = 33;
            this.label2.Text = "Khi đến hạn";
            // 
            // cBTraLai
            // 
            this.cBTraLai.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBTraLai.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cBTraLai.FormattingEnabled = true;
            this.cBTraLai.Items.AddRange(new object[] {
            "Cuối kỳ",
            "Đầu kỳ",
            "Định kỳ hàng tháng"});
            this.cBTraLai.Location = new System.Drawing.Point(426, 90);
            this.cBTraLai.Name = "cBTraLai";
            this.cBTraLai.Size = new System.Drawing.Size(163, 24);
            this.cBTraLai.TabIndex = 32;
            // 
            // cBKhiDenHan
            // 
            this.cBKhiDenHan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBKhiDenHan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cBKhiDenHan.FormattingEnabled = true;
            this.cBKhiDenHan.Items.AddRange(new object[] {
            "Tái tục gốc và lãi",
            "Tái tục gốc",
            "Tất toán sổ"});
            this.cBKhiDenHan.Location = new System.Drawing.Point(153, 90);
            this.cBKhiDenHan.Name = "cBKhiDenHan";
            this.cBKhiDenHan.Size = new System.Drawing.Size(163, 24);
            this.cBKhiDenHan.TabIndex = 31;
            // 
            // txtLaiSuat
            // 
            this.txtLaiSuat.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtLaiSuat.Location = new System.Drawing.Point(426, 64);
            this.txtLaiSuat.Name = "txtLaiSuat";
            this.txtLaiSuat.Size = new System.Drawing.Size(163, 22);
            this.txtLaiSuat.TabIndex = 29;
            // 
            // txtLaiSuatKhongKyHan
            // 
            this.txtLaiSuatKhongKyHan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtLaiSuatKhongKyHan.Location = new System.Drawing.Point(153, 64);
            this.txtLaiSuatKhongKyHan.Name = "txtLaiSuatKhongKyHan";
            this.txtLaiSuatKhongKyHan.Size = new System.Drawing.Size(163, 22);
            this.txtLaiSuatKhongKyHan.TabIndex = 28;
            // 
            // txtSoTienGui
            // 
            this.txtSoTienGui.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtSoTienGui.Location = new System.Drawing.Point(426, 38);
            this.txtSoTienGui.Name = "txtSoTienGui";
            this.txtSoTienGui.Size = new System.Drawing.Size(163, 22);
            this.txtSoTienGui.TabIndex = 27;
            this.txtSoTienGui.TextChanged += new System.EventHandler(this.txtSoTienGui_TextChanged);
            // 
            // dTNgayGui
            // 
            this.dTNgayGui.CustomFormat = "MM/dd/yyyy";
            this.dTNgayGui.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.dTNgayGui.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dTNgayGui.Location = new System.Drawing.Point(153, 10);
            this.dTNgayGui.Margin = new System.Windows.Forms.Padding(2);
            this.dTNgayGui.Name = "dTNgayGui";
            this.dTNgayGui.Size = new System.Drawing.Size(163, 22);
            this.dTNgayGui.TabIndex = 46;
            // 
            // btnThemNganHang
            // 
            this.btnThemNganHang.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnThemNganHang.Image = ((System.Drawing.Image)(resources.GetObject("btnThemNganHang.Image")));
            this.btnThemNganHang.Location = new System.Drawing.Point(562, 11);
            this.btnThemNganHang.Name = "btnThemNganHang";
            this.btnThemNganHang.Size = new System.Drawing.Size(27, 23);
            this.btnThemNganHang.TabIndex = 47;
            this.btnThemNganHang.UseVisualStyleBackColor = true;
            this.btnThemNganHang.Click += new System.EventHandler(this.btnThemNganHang_Click);
            // 
            // FrmThemSoTietKiem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 192);
            this.ControlBox = false;
            this.Controls.Add(this.btnThemNganHang);
            this.Controls.Add(this.dTNgayGui);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cBNganHang1);
            this.Controls.Add(this.btnThem);
            this.Controls.Add(this.btnHuy);
            this.Controls.Add(this.cBKyHan);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cBTraLai);
            this.Controls.Add(this.cBKhiDenHan);
            this.Controls.Add(this.txtLaiSuat);
            this.Controls.Add(this.txtLaiSuatKhongKyHan);
            this.Controls.Add(this.txtSoTienGui);
            this.Name = "FrmThemSoTietKiem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thêm sổ tiết kiệm";
            this.Load += new System.EventHandler(this.FrmThemSoTietKiem_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cBNganHang1;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.ComboBox cBKyHan;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cBTraLai;
        private System.Windows.Forms.ComboBox cBKhiDenHan;
        private System.Windows.Forms.TextBox txtLaiSuat;
        private System.Windows.Forms.TextBox txtLaiSuatKhongKyHan;
        private System.Windows.Forms.TextBox txtSoTienGui;
        private System.Windows.Forms.DateTimePicker dTNgayGui;
        private System.Windows.Forms.Button btnThemNganHang;
    }
}