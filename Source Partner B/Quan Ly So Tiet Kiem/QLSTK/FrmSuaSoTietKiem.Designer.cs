﻿namespace QLSTK
{
    partial class FrmSuaSoTietKiem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSuaSoTietKiem));
            this.dTNgayGui = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.cBNganHang1 = new System.Windows.Forms.ComboBox();
            this.btnCapNhat = new System.Windows.Forms.Button();
            this.btnHuy = new System.Windows.Forms.Button();
            this.cBKyHan = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cBTraLai = new System.Windows.Forms.ComboBox();
            this.cBKhiDenHan = new System.Windows.Forms.ComboBox();
            this.txtLaiSuat = new System.Windows.Forms.TextBox();
            this.txtLaiSuatKhongKyHan = new System.Windows.Forms.TextBox();
            this.txtSoTienGui = new System.Windows.Forms.TextBox();
            this.lblMaSoTk = new System.Windows.Forms.Label();
            this.btnThemNganHang = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dTNgayGui
            // 
            this.dTNgayGui.CustomFormat = "MM/dd/yyyy";
            this.dTNgayGui.Enabled = false;
            this.dTNgayGui.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.dTNgayGui.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dTNgayGui.Location = new System.Drawing.Point(142, 46);
            this.dTNgayGui.Margin = new System.Windows.Forms.Padding(2);
            this.dTNgayGui.Name = "dTNgayGui";
            this.dTNgayGui.Size = new System.Drawing.Size(163, 22);
            this.dTNgayGui.TabIndex = 65;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(1, 202);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(439, 16);
            this.label1.TabIndex = 64;
            this.label1.Text = "Nếu không nhập lãi suất không kỳ hạn, thì mặc định lãi suất là 0.05%/năm";
            // 
            // cBNganHang1
            // 
            this.cBNganHang1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBNganHang1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cBNganHang1.FormattingEnabled = true;
            this.cBNganHang1.Location = new System.Drawing.Point(399, 46);
            this.cBNganHang1.Name = "cBNganHang1";
            this.cBNganHang1.Size = new System.Drawing.Size(130, 24);
            this.cBNganHang1.TabIndex = 63;
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnCapNhat.Location = new System.Drawing.Point(207, 175);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(75, 23);
            this.btnCapNhat.TabIndex = 62;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.UseVisualStyleBackColor = true;
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnHuy
            // 
            this.btnHuy.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnHuy.Location = new System.Drawing.Point(332, 175);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(75, 23);
            this.btnHuy.TabIndex = 61;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.UseVisualStyleBackColor = true;
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // cBKyHan
            // 
            this.cBKyHan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBKyHan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cBKyHan.FormattingEnabled = true;
            this.cBKyHan.Items.AddRange(new object[] {
            "không kỳ hạn",
            "1 tháng",
            "3 tháng",
            "6 tháng",
            "12 tháng"});
            this.cBKyHan.Location = new System.Drawing.Point(142, 72);
            this.cBKyHan.Name = "cBKyHan";
            this.cBKyHan.Size = new System.Drawing.Size(163, 24);
            this.cBKyHan.TabIndex = 60;
            this.cBKyHan.SelectedIndexChanged += new System.EventHandler(this.cBKyHan_SelectedIndexChanged_1);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label9.Location = new System.Drawing.Point(329, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 16);
            this.label9.TabIndex = 59;
            this.label9.Text = "Ngân hàng";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label8.Location = new System.Drawing.Point(1, 103);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 16);
            this.label8.TabIndex = 58;
            this.label8.Text = "Lãi suất không kỳ hạn";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label7.Location = new System.Drawing.Point(73, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 16);
            this.label7.TabIndex = 57;
            this.label7.Text = "Kỳ hạn";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label6.Location = new System.Drawing.Point(64, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 16);
            this.label6.TabIndex = 56;
            this.label6.Text = "Ngày gửi";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label5.Location = new System.Drawing.Point(350, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 16);
            this.label5.TabIndex = 55;
            this.label5.Text = "Trả lãi";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label4.Location = new System.Drawing.Point(345, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 16);
            this.label4.TabIndex = 54;
            this.label4.Text = "Lãi suất";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label3.Location = new System.Drawing.Point(329, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 53;
            this.label3.Text = "Số tiền gửi";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.Location = new System.Drawing.Point(48, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 16);
            this.label2.TabIndex = 52;
            this.label2.Text = "Khi đến hạn";
            // 
            // cBTraLai
            // 
            this.cBTraLai.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBTraLai.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cBTraLai.FormattingEnabled = true;
            this.cBTraLai.Items.AddRange(new object[] {
            "Cuối kỳ",
            "Đầu kỳ",
            "Định kỳ hàng tháng"});
            this.cBTraLai.Location = new System.Drawing.Point(399, 126);
            this.cBTraLai.Name = "cBTraLai";
            this.cBTraLai.Size = new System.Drawing.Size(163, 24);
            this.cBTraLai.TabIndex = 51;
            // 
            // cBKhiDenHan
            // 
            this.cBKhiDenHan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBKhiDenHan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cBKhiDenHan.FormattingEnabled = true;
            this.cBKhiDenHan.Items.AddRange(new object[] {
            "Tái tục gốc và lãi",
            "Tái tục gốc",
            "Tất toán sổ"});
            this.cBKhiDenHan.Location = new System.Drawing.Point(142, 126);
            this.cBKhiDenHan.Name = "cBKhiDenHan";
            this.cBKhiDenHan.Size = new System.Drawing.Size(163, 24);
            this.cBKhiDenHan.TabIndex = 50;
            // 
            // txtLaiSuat
            // 
            this.txtLaiSuat.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtLaiSuat.Location = new System.Drawing.Point(399, 100);
            this.txtLaiSuat.Name = "txtLaiSuat";
            this.txtLaiSuat.Size = new System.Drawing.Size(163, 22);
            this.txtLaiSuat.TabIndex = 49;
            // 
            // txtLaiSuatKhongKyHan
            // 
            this.txtLaiSuatKhongKyHan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtLaiSuatKhongKyHan.Location = new System.Drawing.Point(142, 100);
            this.txtLaiSuatKhongKyHan.Name = "txtLaiSuatKhongKyHan";
            this.txtLaiSuatKhongKyHan.Size = new System.Drawing.Size(163, 22);
            this.txtLaiSuatKhongKyHan.TabIndex = 48;
            // 
            // txtSoTienGui
            // 
            this.txtSoTienGui.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtSoTienGui.Location = new System.Drawing.Point(399, 74);
            this.txtSoTienGui.Name = "txtSoTienGui";
            this.txtSoTienGui.Size = new System.Drawing.Size(163, 22);
            this.txtSoTienGui.TabIndex = 47;
            this.txtSoTienGui.TextChanged += new System.EventHandler(this.txtSoTienGui_TextChanged_1);
            // 
            // lblMaSoTk
            // 
            this.lblMaSoTk.AutoSize = true;
            this.lblMaSoTk.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblMaSoTk.Location = new System.Drawing.Point(12, 9);
            this.lblMaSoTk.Name = "lblMaSoTk";
            this.lblMaSoTk.Size = new System.Drawing.Size(102, 16);
            this.lblMaSoTk.TabIndex = 66;
            this.lblMaSoTk.Text = "Mã sổ tiết kiệm:";
            // 
            // btnThemNganHang
            // 
            this.btnThemNganHang.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnThemNganHang.Image = ((System.Drawing.Image)(resources.GetObject("btnThemNganHang.Image")));
            this.btnThemNganHang.Location = new System.Drawing.Point(535, 46);
            this.btnThemNganHang.Name = "btnThemNganHang";
            this.btnThemNganHang.Size = new System.Drawing.Size(27, 23);
            this.btnThemNganHang.TabIndex = 67;
            this.btnThemNganHang.UseVisualStyleBackColor = true;
            this.btnThemNganHang.Click += new System.EventHandler(this.btnThemNganHang_Click);
            // 
            // FrmSuaSoTietKiem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 224);
            this.ControlBox = false;
            this.Controls.Add(this.btnThemNganHang);
            this.Controls.Add(this.lblMaSoTk);
            this.Controls.Add(this.dTNgayGui);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cBNganHang1);
            this.Controls.Add(this.btnCapNhat);
            this.Controls.Add(this.btnHuy);
            this.Controls.Add(this.cBKyHan);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cBTraLai);
            this.Controls.Add(this.cBKhiDenHan);
            this.Controls.Add(this.txtLaiSuat);
            this.Controls.Add(this.txtLaiSuatKhongKyHan);
            this.Controls.Add(this.txtSoTienGui);
            this.Name = "FrmSuaSoTietKiem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sửa sổ tiết kiệm";
            this.Load += new System.EventHandler(this.FrmSuaSoTietKiem_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dTNgayGui;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cBNganHang1;
        private System.Windows.Forms.Button btnCapNhat;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.ComboBox cBKyHan;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cBTraLai;
        private System.Windows.Forms.ComboBox cBKhiDenHan;
        private System.Windows.Forms.TextBox txtLaiSuat;
        private System.Windows.Forms.TextBox txtLaiSuatKhongKyHan;
        private System.Windows.Forms.TextBox txtSoTienGui;
        private System.Windows.Forms.Label lblMaSoTk;
        private System.Windows.Forms.Button btnThemNganHang;
    }
}