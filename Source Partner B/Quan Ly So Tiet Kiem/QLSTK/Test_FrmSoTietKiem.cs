﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Extensions.Forms;

namespace QLSTK
{    
        [TestFixture]
        class Test_FrmSoTietKiem
        {
        private FormSoTietKiem frm_sotietkiem;


        [SetUp]
        public void ChayFormSTK()
        {
            frm_sotietkiem = new FormSoTietKiem();
            frm_sotietkiem.Show();
        }

        [Test]
            public void TestKhoiTaoFormSoTietKiem()
            {
            Assert.AreEqual("Sổ tiết kiệm", frm_sotietkiem.Text);
            }

        [Test]
        public void TestLabel_DSSTK()
        {
            ChayFormSTK();
            LabelTester lblSoTietKiem = new LabelTester("label3", frm_sotietkiem);
            Assert.AreEqual("SỔ TIẾT KIỆM",lblSoTietKiem.Text);

            LabelTester lblTongTienAllNganHang = new LabelTester("lblTongAllNganHang", frm_sotietkiem);
            Assert.AreEqual("Tổng tiền trong sổ:  VNĐ", lblTongTienAllNganHang.Text);

            LabelTester lblTongTienTheoTungNganHang = new LabelTester("lblTongTienNH", frm_sotietkiem);
            Assert.AreEqual("Ngân Hàng ACB:  VNĐ", lblTongTienTheoTungNganHang.Text);


        }


        [Test]
        public void TestBtn_DSSTK()
            {
                ChayFormSTK();

                ButtonTester btnThem = new ButtonTester("btnThem", frm_sotietkiem);
                Assert.AreEqual("Thêm", btnThem.Text);

                ButtonTester btnSua = new ButtonTester("btnSua", frm_sotietkiem);
                Assert.AreEqual("Sửa", btnSua.Text);

                ButtonTester btnGuiThem = new ButtonTester("btnGuiThem", frm_sotietkiem);
                Assert.AreEqual("Gửi Thêm", btnGuiThem.Text);

                ButtonTester btnRut1Phan = new ButtonTester("btnRutMotPhan", frm_sotietkiem);
                Assert.AreEqual("Rút 1 Phần", btnRut1Phan.Text);

                ButtonTester btnTatToan = new ButtonTester("btnTatToan", frm_sotietkiem);
                Assert.AreEqual("Tất Toán", btnTatToan.Text);

                ButtonTester btnDangXuat = new ButtonTester("btnDangXuat", frm_sotietkiem);
                Assert.AreEqual("Đăng Xuất", btnDangXuat.Text);


        }


        
    }

}
