﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Extensions.Forms;
using NUnit.Framework;

namespace QLSTK
{
    [TestFixture]
    class Test_FrmRutMotPhan
    {
        private FrmRutMotPhan frm;


        [SetUp]
        public void ChayFormRut1phan()
        {
            frm = new FrmRutMotPhan();
            frm.Show();
        }

        [Test]
        public void TestKhoiTaoFormSoTietKiem()
        {
            Assert.AreEqual("Rút 1 Phần", frm.Text);
        }

       


        [Test]
        public void TestBtn_Rut1Phan()
        {
            ChayFormRut1phan();

            ButtonTester btnruttien = new ButtonTester("btnRutTien", frm);
            Assert.AreEqual("Rút tiền", btnruttien.Text);

            ButtonTester btnHuy = new ButtonTester("btnHuy", frm);
            Assert.AreEqual("Hủy", btnHuy.Text);


        }

        [Test] // test so tien rut nhap chu
        public void Test_FrmRutMotPhan_NhapSoTienRut_UTC1()
        {
            Assert.AreEqual(false, frm.KiemTraNhapSoTienRut("abc"), "Số tiền rút phải nhập bằng số!");
        }

        [Test] // test so tien rut nhap chu va so
        public void Test_FrmRutMotPhan_NhapSoTienRut_UTC2()
        {
            Assert.AreEqual(false, frm.KiemTraNhapSoTienRut("5000000abc"), "Số tiền rút phải nhập bằng số!");
        }

        [Test] // test so tien rut nhap so
        public void Test_FrmRutMotPhan_NhapSoTienRut_UTC3()
        {
            Assert.AreEqual(true, frm.KiemTraNhapSoTienRut("500000"), "Rút thành công!");
        }


        [Test] // test so tien rut bo trong
        public void Test_FrmRutMotPhan_NhapSoTienRut_UTC4()
        {
            Assert.AreEqual(false, frm.KiemTraNhapSoTienRut(""), "Số tiền rút không được bỏ trống!");
        }


    }
}
