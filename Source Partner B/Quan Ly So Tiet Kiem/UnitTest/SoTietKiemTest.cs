﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Extensions.Forms;

namespace UnitTest
{
    [TestFixture]
    class SoTietKiemTest
    {
        private QLSTK.FormSoTietKiem frm_sotietkiem;


        [SetUp]
        public void ChayFormSTK()
        {
            frm_sotietkiem = new FormSoTietKiem();
            frm_sotietkiem.Show();
        }

        [Test]
        public void TestKhoiTaoFormSoTietKiem()
        {
            Assert.AreEqual("Sổ tiết kiệm", frm_sotietkiem.Text);
        }

        [Test]
        public void TestLabel()
        {
            ChayFormSTK();
            LabelTester lblSoTietKiem = new LabelTester("label3", frm_sotietkiem);
            Assert.AreEqual("SỔ TIẾT KIỆM", lblSoTietKiem.Text);

            LabelTester lblTongTienAllNganHang = new LabelTester("lblTongAllNganHang", frm_sotietkiem);
            Assert.AreEqual("Tổng tiền trong sổ:  VNĐ", lblTongTienAllNganHang.Text);

            LabelTester lblTongTienTheoTungNganHang = new LabelTester("lblTongTienNH", frm_sotietkiem);
            Assert.AreEqual("Ngân Hàng ACB:  VNĐ", lblTongTienTheoTungNganHang.Text);


        }

        [Test]
        public void Test()
        {
            //Assert.AreEqual(5, _cal.Add(4, 1));
        }
    }
}
