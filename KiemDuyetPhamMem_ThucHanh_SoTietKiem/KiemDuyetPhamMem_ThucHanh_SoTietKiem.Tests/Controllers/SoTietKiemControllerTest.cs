﻿using System;
using System.Linq;
using System.Web.Mvc;
using KiemDuyetPhamMem_ThucHanh_SoTietKiem.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace KiemDuyetPhamMem_ThucHanh_SoTietKiem.Tests.Controllers
{
    [TestClass]
    public class SoTietKiemControllerTest
    {
        readonly string thongbaothanhcong = "<script>document.getElementById('thongbaoThanhCong').style.display = 'block';</script>";
        readonly string thongbaothatbai = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";

        Entities db = new Entities();
        [TestMethod]
        public void TestRutMotPhanView()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var soTietKiems = db.SoTietKiems;
            foreach (var item in soTietKiems)
            {
                ViewResult result = controller.RutMotPhan(item.IdSoTietKiem) as ViewResult;
                Assert.IsNotNull(result);
            }
        }

        [TestMethod] //tienRut = null | hiện thông báo "Tiền rút không thể để trống"
        public void UTCID01()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var soTietKiems = db.SoTietKiems;
            foreach (var item in soTietKiems)
            {
                DateTime ngayGui = (DateTime)item.NgayGui;
                var demNgay = (DateTime.Now - ngayGui).TotalDays;
                var result = controller.RutMotPhan(item.IdSoTietKiem, null) as ViewResult;
                if (item.TinhTrang == "Đã tất toán")
                {
                    Assert.AreEqual("Sổ đã tất toán , không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if (demNgay < 15 && item.KyHan == "không kỳ hạn")
                {
                    Assert.AreEqual("không thể rút tiền,chỉ có thể rút những sổ đã tồn tại 15 ngày", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if (item.SoTienGoc < 100000 && item.KyHan == "không kỳ hạn")
                {
                    Assert.AreEqual("sổ dưới 100000 không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if ((item.KyHan == "1 tháng" || item.KyHan == "3 tháng" || item.KyHan == "6 tháng" || item.KyHan == "12 tháng") && item.SoTienGoc < 100000)
                {
                    Assert.AreEqual("sổ dưới 100000 không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else
                {
                    Assert.IsNotNull(result);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Tiền rút không thể để trống", result.ViewBag.ErrorTienRut);
                    Assert.AreEqual(item.IdSoTietKiem, result.ViewBag.IdSoTietKiem);
                    Assert.AreEqual(null, result.ViewBag.TienRut);
                }
            }
        }
        [TestMethod] //tienRut > 9999999999999 | hiện thông báo "số tiền vượt quá giới hạn cho phép"
        public void UTCID02()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var soTietKiems = db.SoTietKiems;
            foreach (var item in soTietKiems)
            {
                DateTime ngayGui = (DateTime)item.NgayGui;
                var demNgay = (DateTime.Now - ngayGui).TotalDays;
                var result = controller.RutMotPhan(item.IdSoTietKiem, 10000000000000) as ViewResult;
                if (item.TinhTrang == "Đã tất toán")
                {
                    Assert.AreEqual("Sổ đã tất toán , không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if (demNgay < 15 && item.KyHan == "không kỳ hạn")
                {
                    Assert.AreEqual("không thể rút tiền,chỉ có thể rút những sổ đã tồn tại 15 ngày", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if (item.SoTienGoc < 100000 && item.KyHan == "không kỳ hạn")
                {
                    Assert.AreEqual("sổ dưới 100000 không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if ((item.KyHan == "1 tháng" || item.KyHan == "3 tháng" || item.KyHan == "6 tháng" || item.KyHan == "12 tháng") && item.SoTienGoc < 100000)
                {
                    Assert.AreEqual("sổ dưới 100000 không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else
                {
                    Assert.IsNotNull(result);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("số tiền vượt quá giới hạn cho phép", result.ViewBag.ErrorTienRut);
                    Assert.AreEqual(item.IdSoTietKiem, result.ViewBag.IdSoTietKiem);
                    Assert.AreEqual(10000000000000, result.ViewBag.TienRut);
                }
            }

        }
        [TestMethod] //tienRut < 0 | hiện thông báo "tiền rút không thể là số âm"
        public void UTCID03()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var soTietKiems = db.SoTietKiems;
            foreach (var item in soTietKiems)
            {
                DateTime ngayGui = (DateTime)item.NgayGui;
                var demNgay = (DateTime.Now - ngayGui).TotalDays;
                var result = controller.RutMotPhan(item.IdSoTietKiem, -1) as ViewResult;
                if (item.TinhTrang == "Đã tất toán")
                {
                    Assert.AreEqual("Sổ đã tất toán , không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if (demNgay < 15 && item.KyHan == "không kỳ hạn")
                {
                    Assert.AreEqual("không thể rút tiền,chỉ có thể rút những sổ đã tồn tại 15 ngày", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if (item.SoTienGoc < 100000 && item.KyHan == "không kỳ hạn")
                {
                    Assert.AreEqual("sổ dưới 100000 không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if ((item.KyHan == "1 tháng" || item.KyHan == "3 tháng" || item.KyHan == "6 tháng" || item.KyHan == "12 tháng") && item.SoTienGoc < 100000)
                {
                    Assert.AreEqual("sổ dưới 100000 không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else
                {
                    Assert.IsNotNull(result);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("tiền rút không thể là số âm", result.ViewBag.ErrorTienRut);
                    Assert.AreEqual(item.IdSoTietKiem, result.ViewBag.IdSoTietKiem);
                    Assert.AreEqual(-1, result.ViewBag.TienRut);
                }
            }
        }
        [TestMethod] //tienRut [0,99999] | hiện thông báo "rút tối thiểu 100.000đ"
        public void UTCID04()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var soTietKiems = db.SoTietKiems;
            foreach (var item in soTietKiems)
            {
                DateTime ngayGui = (DateTime)item.NgayGui;
                var demNgay = (DateTime.Now - ngayGui).TotalDays;
                for (int i = 0; i < 100000; i = i + 5000)
                {
                    var result = controller.RutMotPhan(item.IdSoTietKiem, i) as ViewResult;
                    if (item.TinhTrang == "Đã tất toán")
                    {
                        Assert.AreEqual("Sổ đã tất toán , không thể rút tiền.", result.TempData["message"]);
                        Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                        Assert.AreEqual("Index", result.ViewName);
                    }
                    else if (demNgay < 15 && item.KyHan == "không kỳ hạn")
                    {
                        Assert.AreEqual("không thể rút tiền,chỉ có thể rút những sổ đã tồn tại 15 ngày", result.TempData["message"]);
                        Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                        Assert.AreEqual("Index", result.ViewName);
                    }
                    else if (item.SoTienGoc < 100000 && item.KyHan == "không kỳ hạn")
                    {
                        Assert.AreEqual("sổ dưới 100000 không thể rút tiền.", result.TempData["message"]);
                        Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                        Assert.AreEqual("Index", result.ViewName);
                    }
                    else if ((item.KyHan == "1 tháng" || item.KyHan == "3 tháng" || item.KyHan == "6 tháng" || item.KyHan == "12 tháng") && item.SoTienGoc < 100000)
                    {
                        Assert.AreEqual("sổ dưới 100000 không thể rút tiền.", result.TempData["message"]);
                        Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                        Assert.AreEqual("Index", result.ViewName);
                    }
                    else
                    {
                        Assert.IsNotNull(result);
                        Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                        Assert.AreEqual("rút tối thiểu 100.000đ", result.ViewBag.ErrorTienRut);
                        Assert.AreEqual(item.IdSoTietKiem, result.ViewBag.IdSoTietKiem);
                        Assert.AreEqual(i, result.ViewBag.TienRut);
                    }
                }
            }
        }
    
        [TestMethod] //tienRut > SoTienGoc | hiện thông báo "tiền rút không thể lớn hơn số tiền gốc"
        public void UTCID05()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var soTietKiems = db.SoTietKiems;
            foreach (var item in soTietKiems)
            {
                DateTime ngayGui = (DateTime)item.NgayGui;
                var demNgay = (DateTime.Now - ngayGui).TotalDays;
                var result = controller.RutMotPhan(item.IdSoTietKiem, item.SoTienGoc+1) as ViewResult;
                if (item.TinhTrang == "Đã tất toán")
                {
                    Assert.AreEqual("Sổ đã tất toán , không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if (demNgay < 15 && item.KyHan == "không kỳ hạn")
                {
                    Assert.AreEqual("không thể rút tiền,chỉ có thể rút những sổ đã tồn tại 15 ngày", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if (item.SoTienGoc < 100000 && item.KyHan == "không kỳ hạn")
                {
                    Assert.AreEqual("sổ dưới 100000 không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if ((item.KyHan == "1 tháng" || item.KyHan == "3 tháng" || item.KyHan == "6 tháng" || item.KyHan == "12 tháng") && item.SoTienGoc < 100000)
                {
                    Assert.AreEqual("sổ dưới 100000 không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else
                {
                    Assert.IsNotNull(result);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("tiền rút không thể lớn hơn số tiền gốc", result.ViewBag.ErrorTienRut);
                    Assert.AreEqual(item.IdSoTietKiem, result.ViewBag.IdSoTietKiem);
                    Assert.AreEqual(item.SoTienGoc + 1, result.ViewBag.TienRut);
                }
            }
        }
        [TestMethod] //tienRut < SoTienGoc |
        public void UTCID06()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var soTietKiems = db.SoTietKiems;
            foreach (var item in soTietKiems)
            {
                DateTime ngayGui = (DateTime)item.NgayGui;
                var demNgay = (DateTime.Now - ngayGui).TotalDays;
                var result = controller.RutMotPhan(item.IdSoTietKiem, item.SoTienGoc -1) as ViewResult;
                if (item.TinhTrang == "Đã tất toán")
                {
                    Assert.AreEqual("Sổ đã tất toán , không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if (demNgay < 15 && item.KyHan == "không kỳ hạn")
                {
                    Assert.AreEqual("không thể rút tiền,chỉ có thể rút những sổ đã tồn tại 15 ngày", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if (item.SoTienGoc < 100000 && item.KyHan == "không kỳ hạn")
                {
                    Assert.AreEqual("sổ dưới 100000 không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else if ((item.KyHan == "1 tháng" || item.KyHan == "3 tháng" || item.KyHan == "6 tháng" || item.KyHan == "12 tháng") && item.SoTienGoc < 100000)
                {
                    Assert.AreEqual("sổ dưới 100000 không thể rút tiền.", result.TempData["message"]);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                }
                else
                {
                    Assert.IsNotNull(result);
                    Assert.AreEqual(thongbaothanhcong, result.TempData["MessageSuccess"]);
                    Assert.AreEqual("Index",result.ViewName);
                }
            }
        }
        [TestMethod] //id = null ; tienRut= null
        public void UTCID07()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var result = controller.RutMotPhan(null, null) as ViewResult;
                Assert.IsNotNull(result);
                Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                Assert.AreEqual("Index", result.ViewName);
                Assert.AreEqual("ID không thể rỗng", result.ViewBag.Message);
        }
        [TestMethod] //id = null ; tienRut > 9999999999999
        public void UTCID08()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var result = controller.RutMotPhan(null, null) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual("ID không thể rỗng", result.ViewBag.Message);
        }
        [TestMethod] //id = null ; tienRut <0
        public void UTCID09()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var result = controller.RutMotPhan(null, null) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual("ID không thể rỗng", result.ViewBag.Message);
        }
        [TestMethod] //id = null ; tienRut [0,99999]
        public void UTCID10()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var result = controller.RutMotPhan(null, null) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual("ID không thể rỗng", result.ViewBag.Message);
        }
        [TestMethod] //id = null ; tienRut > SoTienGoc
        public void UTCID11()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var result = controller.RutMotPhan(null, null) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual("ID không thể rỗng", result.ViewBag.Message);
        }
        [TestMethod] //id = null ; tienRut < SoTienGoc
        public void UTCID12()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var result = controller.RutMotPhan(null, null) as ViewResult;
                Assert.IsNotNull(result);
                Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                Assert.AreEqual("Index", result.ViewName);
                Assert.AreEqual("ID không thể rỗng", result.ViewBag.Message);
        }
        [TestMethod] //id = null ; tienRut = null
        public void UTCID13()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var listSoTietKiem = db.SoTietKiems.ToList();
            int idkhongtontai = 1;
            foreach(var item in listSoTietKiem)
            {
                if (idkhongtontai == item.IdSoTietKiem)
                {
                    idkhongtontai++;
                }
            }
            var result = controller.RutMotPhan(idkhongtontai,null) as ViewResult;
                    Assert.IsNotNull(result);
                    Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                    Assert.AreEqual("Index", result.ViewName);
                    Assert.AreEqual("ID không tồn tại", result.ViewBag.Message);
        }
        [TestMethod] //id = null ; tienRut > max.decimal(13,0)
        public void UTCID14()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var listSoTietKiem = db.SoTietKiems.ToList();
            int idkhongtontai = 1;
            foreach (var item in listSoTietKiem)
            {
                if (idkhongtontai == item.IdSoTietKiem)
                {
                    idkhongtontai++;
                }
            }

            var result = controller.RutMotPhan(idkhongtontai, 10000000000000) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual("ID không tồn tại", result.ViewBag.Message);
        }
        [TestMethod] //id = null ; tienRut âm
        public void UTCID15()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var listSoTietKiem = db.SoTietKiems.ToList();
            int idkhongtontai = 1;
            foreach (var item in listSoTietKiem)
            {
                if (idkhongtontai == item.IdSoTietKiem)
                {
                    idkhongtontai++;
                }
            }

            var result = controller.RutMotPhan(idkhongtontai, -1) as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual("ID không tồn tại", result.ViewBag.Message);
        }
        [TestMethod] //id = null ; tienRut [0,99999]
        public void UTCID16()
        {
            SoTietKiemsController controller = new SoTietKiemsController();
            var listSoTietKiem = db.SoTietKiems.ToList();
            int idkhongtontai = 1;
            foreach (var item in listSoTietKiem)
            {
                if (idkhongtontai == item.IdSoTietKiem)
                {
                    idkhongtontai++;
                }
            }
            for (int i = 0; i < 100000; i = i + 5000)
            {
                var result = controller.RutMotPhan(idkhongtontai, i) as ViewResult;
                Assert.IsNotNull(result);
                Assert.AreEqual(thongbaothatbai, result.TempData["MessageWarning"]);
                Assert.AreEqual("Index", result.ViewName);
                Assert.AreEqual("ID không tồn tại", result.ViewBag.Message);
            }   
        }
            
    }
}
