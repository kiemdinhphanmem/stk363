﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KiemDuyetPhamMem_ThucHanh_SoTietKiem;

namespace KiemDuyetPhamMem_ThucHanh_SoTietKiem.Controllers
{
    public class SoTietKiemsController : Controller
    {
        private Entities db = new Entities();

        // GET: SoTietKiems
        public ActionResult Index()
        {
            var soTietKiems = db.SoTietKiems.Include(s => s.AspNetUser);
            return View(soTietKiems.ToList());
        }

        // GET: SoTietKiems/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                SoTietKiem soTietKiem = db.SoTietKiems.Find(id);
                if (soTietKiem == null)
                {
                    return HttpNotFound();
                }
                ViewBag.IdSoTietKiem = id;
                return View(soTietKiem);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            
        }

        // GET: SoTietKiems/Create
        public ActionResult Create()
        {
            ViewBag.Id_User = new SelectList(db.AspNetUsers, "Id", "Email");
            return View();
        }

        // POST: SoTietKiems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdSoTietKiem,Id_User,TenNganHang,NgayGui,SoTienGoc,KyHan,LaiSuat,LaiSuatKhongKyHan,TraLai,KhiDenHan,TinhTrang")] SoTietKiem soTietKiem)
        {
            if (ModelState.IsValid)
            {
                if (soTietKiem.LaiSuatKhongKyHan == null)
                {
                    soTietKiem.LaiSuatKhongKyHan = 0.05;
                }
                db.SoTietKiems.Add(soTietKiem);
                db.SaveChanges();
                TempData["message"] = "<b>Thêm thành công</b>:<br> ID sổ: " + soTietKiem.TenNganHang + "_" + soTietKiem.IdSoTietKiem + "<br>Số tiền gốc: " + soTietKiem.SoTienGoc + "<br>Kỳ hạn: " + soTietKiem.KyHan;
                TempData["MessageSuccess"] = "<script>document.getElementById('thongbaoThanhCong').style.display = 'block';</script>";
                return View("Index");
            }
            ViewBag.Id_User = new SelectList(db.AspNetUsers, "Id", "Email", soTietKiem.Id_User);
            return View(soTietKiem);
        }
        public ActionResult Them1()
        {
            ViewBag.Id_User = new SelectList(db.AspNetUsers, "Id", "Email");
            return View();
        }

        // POST: SoTietKiems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Them1(string tenNganHang , DateTime ngaygui,string id_user)
        {
            try
            {
                SoTietKiem soTietKiem = new SoTietKiem();
                soTietKiem.Id_User = id_user;
                soTietKiem.TenNganHang = tenNganHang;
                soTietKiem.NgayGui = ngaygui;


                soTietKiem.TinhTrang = "Chưa hoàn thiện";
                soTietKiem.SoTienGoc = 0;
                soTietKiem.KyHan = "";
                soTietKiem.LaiSuat = 0;

                soTietKiem.LaiSuatKhongKyHan = 0;
                soTietKiem.TraLai = "";
                soTietKiem.KhiDenHan = "";

                soTietKiem.TienLai = 0;

            
                db.SoTietKiems.Add(soTietKiem);
                db.SaveChanges();
                TempData["message"] = "<b>lưu thành công</b>";
                TempData["MessageSuccess"] = "<script>document.getElementById('thongbaoThanhCong').style.display = 'block';</script>";
                ViewBag.IdSoTietKiem = soTietKiem.IdSoTietKiem;
                return View("Them2",soTietKiem);
            }
            catch
            {
                TempData["message"] = "Thao tác thất bại";
                TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                return RedirectToAction("Index");
            }

        }
        public ActionResult Them2(int? id)
        {
            try
            {
                SoTietKiem soTietKiem = db.SoTietKiems.Find(id);
                if (soTietKiem == null)
                {
                    TempData["message"] = "Thao tác thất bại";
                    TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                    return View("Index");
                }
                ViewBag.IdSoTietKiem = id;
                return View(soTietKiem);
            }
            catch
            {
                TempData["message"] = "Thao tác thất bại";
                TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                return RedirectToAction("Index");
            }
        }

        // POST: SoTietKiems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Them2(int id_sotietkiem,decimal soTienGoc, string kyHan, double laiSuat)
        {
            try
            {
                SoTietKiem soTietKiem = db.SoTietKiems.Find(id_sotietkiem);
                soTietKiem.SoTienGoc = soTienGoc;
                soTietKiem.KyHan = kyHan;
                soTietKiem.LaiSuat = laiSuat;
            
                db.Entry(soTietKiem).State = EntityState.Modified;
                db.SaveChanges();
                TempData["message"] = "<b>lưu thành công</b>";
                TempData["MessageSuccess"] = "<script>document.getElementById('thongbaoThanhCong').style.display = 'block';</script>";
                ViewBag.IdSoTietKiem = soTietKiem.IdSoTietKiem;
                return View("Them3", soTietKiem);
            }
            catch
            {
                TempData["message"] = "Thao tác thất bại";
                TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                return RedirectToAction("Index");
            }

        }
        public ActionResult Them3(int? id)
        {
            try
            {
                SoTietKiem soTietKiem = db.SoTietKiems.Find(id);
                if (soTietKiem == null)
                {
                    TempData["message"] = "Thao tác thất bại";
                    TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                    return View("Index");
                }
                ViewBag.IdSoTietKiem = id;
                return View(soTietKiem);
            }
            catch
            {
                TempData["message"] = "Thao tác thất bại";
                TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                return RedirectToAction("Index");
            }
        }

        // POST: SoTietKiems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Them3(int id_sotietkiem, double laiSuatKhongKyHan,string traLai, string khiDenHan)
        {
            try
            {
                SoTietKiem soTietKiem = db.SoTietKiems.Find(id_sotietkiem);
                soTietKiem.LaiSuatKhongKyHan = laiSuatKhongKyHan;
                soTietKiem.TraLai = traLai;
                soTietKiem.KhiDenHan = khiDenHan;
                soTietKiem.TinhTrang = "Mở"; 
            
                db.Entry(soTietKiem).State = EntityState.Modified;
                db.SaveChanges();
                TempData["message"] = "<b>Thêm thành công</b>:<br> ID sổ: " + soTietKiem.TenNganHang + "_" + soTietKiem.IdSoTietKiem + "<br>Số tiền gốc: " + soTietKiem.SoTienGoc + "<br>Kỳ hạn: " + soTietKiem.KyHan;
                TempData["MessageSuccess"] = "<script>document.getElementById('thongbaoThanhCong').style.display = 'block';</script>";
                ViewBag.IdSoTietKiem = soTietKiem.IdSoTietKiem;
                return RedirectToAction("Index");
            }
            catch
            {
                TempData["message"] = "Thao tác thất bại";
                TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                return RedirectToAction("Index");
            }

        }
        public ActionResult CreateNganHang()
        {
            
            return View();
        }

        // POST: SoTietKiems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNganHang(string tenNganHangKhac,string id_user)
        {
            NganHangCuaUser nganhangmoi = new NganHangCuaUser();
            nganhangmoi.TenNganHang = tenNganHangKhac;
            nganhangmoi.IdUser = id_user;
            db.NganHangCuaUsers.Add(nganhangmoi);
            db.SaveChanges();
            TempData["message"] = "<b>Thêm thành công</b>:<br> tên ngân hàng :"+tenNganHangKhac;
            TempData["MessageSuccess"] = "<script>document.getElementById('thongbaoThanhCong').style.display = 'block';</script>";
            return View("Index");
        }

        // GET: SoTietKiems/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {
                SoTietKiem soTietKiem = db.SoTietKiems.Find(id);
                ViewBag.Id_User = new SelectList(db.AspNetUsers, "Id", "Email", soTietKiem.Id_User);
                ViewBag.IdSoTietKiem = id;
                return View(soTietKiem);
            }
            catch
            {
                TempData["message"] = "Thao tác thất bại<br>Không thể mở trang Edit";
                TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                return View("Index");
            }
            
        }

        // POST: SoTietKiems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdSoTietKiem,Id_User,TenNganHang,NgayGui,SoTienGoc,KyHan,LaiSuat,LaiSuatKhongKyHan,TraLai,KhiDenHan,TinhTrang")] SoTietKiem soTietKiem)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(soTietKiem).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["MessageSuccess"] = "<script>document.getElementById('thongbaoThanhCong').style.display = 'block';</script>";
                    return View("Index");
                }
                ViewBag.Id_User = new SelectList(db.AspNetUsers, "Id", "Email", soTietKiem.Id_User);
                return View(soTietKiem);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotAcceptable);
            }
            
        }

        // GET: SoTietKiems/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                SoTietKiem soTietKiem = db.SoTietKiems.Find(id);
                if (soTietKiem == null)
                {
                    return HttpNotFound();
                }
                return View(soTietKiem);
            }
            catch
            {
                TempData["message"] = "Thao tác thất bại";
                TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                return RedirectToAction("Index");
            }
        }

        // POST: SoTietKiems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SoTietKiem soTietKiem = db.SoTietKiems.Find(id);
            db.SoTietKiems.Remove(soTietKiem);
            db.SaveChanges();
            TempData["message"] = "xóa thành công";
            TempData["MessageSuccess"] = "<script>document.getElementById('thongbaoThanhCong').style.display = 'block';</script>";
            return View("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult ViewUserSoTietKiem()
        {
            return View();
        }
        public ActionResult TatToanSo(int? id)
        {
            try
            {
                SoTietKiem soTietKiem = db.SoTietKiems.Find(id);
                ViewBag.Id_User = new SelectList(db.AspNetUsers, "Id", "Email", soTietKiem.Id_User);
                ViewBag.IdSoTietKiem = id;
                return View(soTietKiem);
            }
            
            catch
            {
                TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                return View("Index");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TatToanSoInput(int? id)
        {
            try
            {
                SoTietKiem soTietKiem = db.SoTietKiems.Find(id);
                //Đếm ngày bắt đầu từ ngày Gửi -> ngày hiện tại
                DateTime ngayGui = (DateTime)soTietKiem.NgayGui;
                decimal demNgay = (int)((DateTime.Now - ngayGui).TotalDays);
                decimal sotiengoc = (decimal)soTietKiem.SoTienGoc;
                decimal laisuatkhongkyhan = (decimal)soTietKiem.LaiSuatKhongKyHan;
                decimal tienlai = Math.Round(((sotiengoc*laisuatkhongkyhan/100/365)*demNgay),0);
                if (ModelState.IsValid)
                {
                    soTietKiem.TinhTrang = "Đã tất toán";
                    soTietKiem.TienLai = tienlai;
                    db.Entry(soTietKiem).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["message"] = "Tất toán thành công:<br> ID sổ: " + soTietKiem.TenNganHang + "_" + soTietKiem.IdSoTietKiem + "<br>Số tiền lãi của bạn là: " + tienlai + "đ";
                    TempData["MessageSuccess"] = "<script>document.getElementById('thongbaoThanhCong').style.display = 'block';</script>";
                    return View("Index");
                }                 
                ViewBag.Id_User = new SelectList(db.AspNetUsers, "Id", "Email", soTietKiem.Id_User);
                return View(soTietKiem);
            }
            catch
            {
                TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                return View("Index");
            }
           
        }
        public ActionResult GuiThem(int? id)
        {
            try
            {
                SoTietKiem soTietKiem = db.SoTietKiems.Find(id);
                ViewBag.Id_User = new SelectList(db.AspNetUsers, "Id", "Email", soTietKiem.Id_User);
                ViewBag.IdSoTietKiem = id;
                return View(soTietKiem);
            }
            catch
            {
                TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                return View("Index");
            }
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GuiThem([Bind(Include = "IdSoTietKiem,Id_User,TenNganHang,NgayGui,SoTienGoc,KyHan,LaiSuat,LaiSuatKhongKyHan,TraLai,KhiDenHan,TinhTrang")] SoTietKiem soTietKiem,[Bind(Include ="TienGuiThem")] int tienGuiThem)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    soTietKiem.SoTienGoc = soTietKiem.SoTienGoc + tienGuiThem;
                    db.Entry(soTietKiem).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["MessageSuccess"] = "<script>document.getElementById('thongbaoThanhCong').style.display = 'block';</script>";
                    return View("Index");
                }
                ViewBag.Id_User = new SelectList(db.AspNetUsers, "Id", "Email", soTietKiem.Id_User);
                return View(soTietKiem);
            }
            catch
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotAcceptable);
            }
            
        }
        public ActionResult RutMotPhan(int? id)
        {
            try
            {

                SoTietKiem soTietKiem = db.SoTietKiems.Find(id);
                var demNgay = (DateTime.Now - (DateTime)soTietKiem.NgayGui).TotalDays;
                foreach (var item in db.SoTietKiems)
                {
                    if(item.IdSoTietKiem==id && item.TinhTrang== "Đã tất toán")
                    {
                        TempData["message"] = "Sổ đã tất toán , không thể rút tiền.";
                        TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                        return View("Index");
                    }
                }
                if (id == null)
                {
                    TempData["message"] = "phát hiện bạn đang cố tính thay đổi URL.<br>id không thể để rỗng";
                    TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                    return View("Index");
                }
                if (id <0)
                {
                    TempData["message"] = "phát hiện bạn đang cố tính thay đổi URL.<br>không tồn tại id là số âm";
                    TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                    return View("Index");
                }
                if (id >= int.MaxValue)
                {
                    TempData["message"] = "phát hiện bạn đang cố tính thay đổi URL.<br>id vượt quá giá trị tối đa";
                    TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                    return View("Index");
                }
                if (soTietKiem.TinhTrang == "Đã tất toán")
                {
                    TempData["message"] = "Sổ đã tất toán , không thể rút tiền.";
                    TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                    return View("Index");
                }
                if (demNgay < 15 && soTietKiem.KyHan == "không kỳ hạn")
                {
                    TempData["message"] = "chỉ có thể rút những sổ đã tồn tại 15 ngày , không thể rút tiền.";
                    TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                    return View("Index");
                }
                if (soTietKiem.SoTienGoc < 100000 && soTietKiem.KyHan == "không kỳ hạn")
                {
                    TempData["message"] = "sổ dưới 100000 không thể rút tiền.";
                    TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                    return View("Index");
                }
                if ((soTietKiem.KyHan == "1 tháng" || soTietKiem.KyHan == "3 tháng" || soTietKiem.KyHan == "6 tháng" || soTietKiem.KyHan == "12 tháng") && soTietKiem.SoTienGoc < 100000)
                {
                    TempData["message"] = "sổ dưới 100000 không thể rút tiền.";
                    TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                    return View("Index");
                }
                ViewBag.Id_User = new SelectList(db.AspNetUsers, "Id", "Email", soTietKiem.Id_User);
                ViewBag.IdSoTietKiem = id;
                return View(soTietKiem);
            }
            catch
            {
                // return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                return View("Index");
            }
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RutMotPhan(int? id,decimal? tienRut)
        {
            try
            {
                if (id == null)
                {
                    ViewBag.Message = "ID không thể rỗng";
                    TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                    return View("Index");
                }
                SoTietKiem soTietKiem = db.SoTietKiems.Find(id);
                if (soTietKiem == null)
                {
                    ViewBag.Message = "ID không tồn tại";
                    TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                    return View("Index");
                }
                //lấy danh sách sổ tiết kiệm trong database
                var demNgay = (DateTime.Now - (DateTime)soTietKiem.NgayGui).TotalDays;
                var listSoTietKiem = db.SoTietKiems.ToList();
                bool IdAvailable = false;
                //kiểm tra id có tồn tại trong database không

                foreach (var item in listSoTietKiem)
                {
                    if (id!=null && item.IdSoTietKiem == id)
                    {
                        IdAvailable = true;
                        if(item.TinhTrang== "Đã tất toán")
                        {
                            TempData["message"] = "Sổ đã tất toán , không thể rút tiền.";
                            TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                            return View("Index");
                        }
                        if (demNgay < 15 && item.KyHan == "không kỳ hạn")
                        {
                            TempData["message"] = "không thể rút tiền,chỉ có thể rút những sổ đã tồn tại 15 ngày";
                            TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                            return View("Index");
                        }
                        if (item.SoTienGoc < 100000 && item.KyHan == "không kỳ hạn" )
                        {
                            TempData["message"] = "sổ dưới 100000 không thể rút tiền.";
                            TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                            return View("Index");
                        }
                        if ((item.KyHan == "1 tháng" || item.KyHan == "3 tháng" || item.KyHan == "6 tháng" || item.KyHan == "12 tháng") && item.SoTienGoc < 100000)
                        {
                            TempData["message"] = "sổ dưới 100000 không thể rút tiền.";
                            TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                            return View("Index");
                        }
                    }
                }
                //id available
                if (IdAvailable)
                {
                    //tienRut = null | hiện thông báo "Tiền rút không thể để trống"
                    if (tienRut == null)
                    {
                        ViewBag.ErrorTienRut = "Tiền rút không thể để trống";
                        TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                        ViewBag.IdSoTietKiem = id;
                        return View();
                    }
                    //tienRut > 9999999999999 | hiện thông báo "số tiền vượt quá giới hạn cho phép"
                    else if (tienRut > 9999999999999)
                    {
                        ViewBag.ErrorTienRut = "số tiền vượt quá giới hạn cho phép";
                        TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                        ViewBag.IdSoTietKiem = id;
                        ViewBag.TienRut = tienRut;
                        return View();
                    }
                    //tienRut < 0 | hiện thông báo "tiền rút không thể là số âm"
                    else if (tienRut < 0)
                    {
                        ViewBag.ErrorTienRut = "tiền rút không thể là số âm";
                        TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                        ViewBag.IdSoTietKiem = id;
                        ViewBag.TienRut = tienRut;
                        return View();
                    }
                    //tienRut [1,99999] | hiện thông báo "rút tối thiểu 100.000đ"
                    else if (tienRut >=0 && tienRut < 100000)
                    {
                        ViewBag.ErrorTienRut = "rút tối thiểu 100.000đ";
                        TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                        ViewBag.IdSoTietKiem = id;
                        ViewBag.TienRut = tienRut;
                        return View();
                    }
                    //tienRut > SoTienGoc | hiện thông báo "tiền rút không thể lớn hơn số tiền gốc"
                    else if (tienRut>=soTietKiem.SoTienGoc)
                    {
                        ViewBag.ErrorTienRut = "tiền rút không thể lớn hơn số tiền gốc";
                        TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                        ViewBag.IdSoTietKiem = id;
                        ViewBag.TienRut = tienRut;
                        return View();
                    }
                }
                if (IdAvailable == false)
                {
                    ViewBag.Message = "ID không tồn tại";
                    TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                    return View("Index");
                }
                
                //decimal tienlai = Math.Round(((decimal)((decimal)tienRut * 
                //                                        (decimal)(soTietKiem.LaiSuatKhongKyHan / 365) * 
                //                                        (decimal)((DateTime.Now - (DateTime)soTietKiem.NgayGui).TotalDays))), 0);
                decimal tienlai = Math.Round((decimal)((double)tienRut * ((soTietKiem.LaiSuatKhongKyHan / 365)/100* (DateTime.Now - (DateTime)soTietKiem.NgayGui).TotalDays)),0);
                soTietKiem.TienLai = soTietKiem.TienLai+tienlai;
                soTietKiem.SoTienGoc = soTietKiem.SoTienGoc - tienRut;
                db.Entry(soTietKiem).State = EntityState.Modified;
                db.SaveChanges();
                TempData["message"] = "sổ " + soTietKiem.TenNganHang + "_" + soTietKiem.IdSoTietKiem + " đã rút thành công " + tienRut.ToString() + "đ<br>tiền lãi của bạn là: " + tienlai + "đ<br> số dư còn:" + soTietKiem.SoTienGoc.ToString();
                TempData["MessageSuccess"] = "<script>document.getElementById('thongbaoThanhCong').style.display = 'block';</script>";
                return View("Index");
            }
            catch 
            {
                // return new HttpStatusCodeResult(HttpStatusCode.BadRequest);.
                ViewBag.Message = "thao tác không hợp lệ";
                TempData["MessageWarning"] = "<script>document.getElementById('thongbaoThatBai').style.display = 'block';</script>";
                return View("Index");
            }
            
        }


    }
}
