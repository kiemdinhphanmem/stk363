﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KiemDuyetPhamMem_ThucHanh_SoTietKiem.Startup))]
namespace KiemDuyetPhamMem_ThucHanh_SoTietKiem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
